<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;

/**
 * Services for list types
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class ListTypeService implements SingletonInterface
{
    /**
     * Converts the list-type into extension-key
     * @return string Extension key or empty string
     */
    public static function checkListType(string $listType): string
    {
        $result = '';
        if (str_starts_with($listType, 'modules_')) {
            $result = 'modules';
        }
        if (str_starts_with($listType, 'isotope_')) {
            $result = 'isotope';
        }
        if (str_starts_with($listType, 'openimmo_')) {
            $result = 'openimmo';
        }
        if (str_starts_with($listType, 'bookings_')) {
            $result = 'bookings';
        }
        if (str_starts_with($listType, 'questions_')) {
            $result = 'questions';
        }
        if (str_starts_with($listType, 'glossaries_')) {
            $result = 'glossaries';
        }
        if (str_starts_with($listType, 'shop_')) {
            $result = 'shop';
        }
        if (str_starts_with($listType, 'poll_')) {
            $result = 'poll';
        }
        if (str_starts_with($listType, 'quiz_')) {
            $result = 'quiz';
        }
        if (str_starts_with($listType, 'rssapp_')) {
            $result = 'rss_app';
        }
        if (str_starts_with($listType, 'portfolios_')) {
            $result = 'portfolios';
        }
        if (str_starts_with($listType, 'newsletters_')) {
            $result = 'newsletters';
        }
        if (str_starts_with($listType, 'imagegallery_')) {
            $result = 'image_gallery';
        }
        if (str_starts_with($listType, 'doubleoptindownload_')) {
            $result = 'double_optin_download';
        }
        if (str_starts_with($listType, 'addressmanager_')) {
            $result = 'address_manager';
        }
        if (str_starts_with($listType, 'jobfinder_')) {
            $result = 'job_finder';
        }
        if (str_starts_with($listType, 'fluidform_')) {
            $result = 'fluid_form';
        }
        return $result;
    }
}
