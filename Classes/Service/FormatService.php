<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;

/**
 * Services for format
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class FormatService implements SingletonInterface
{
    /**
     * Convert amount of seconds to a formatted string.
     * For example:
     * value 3600 h:i -> return 1:00
     * value 3600 h:i:s -> return 1:00:00
     * value 60 h:i -> return 0:01
     * value 60 h:i:s -> return 0:01:00
     * value 59 h:i -> return 0:01
     * value 59 h:i:s -> return 0:00:59
     *
     * @param int $value
     * @param string $format
     * @return string
     */
    public static function formatTime(int $value = 0, string $format = 'h:i'): string
    {
        $result = '0:00';
        if ($value < 0) {
            $value = 0;
        }
        $converted = [
            'hours' => floor($value / 3600),
            'minutes' => floor(($value / 60) % 60),
            'seconds' => ($value % 60)
        ];
        if ($format === 'H:i') {
            $format = 'h:i';
        }
        switch ($format) {
            case 'h:i':
                $result = sprintf('%02d:%02d', $converted['hours'], $converted['minutes']);
                if (trim($result) === '') {
                    $result = '0:00';
                }
                break;
            case 'h:i:s':
                $result = sprintf('%02d:%02d:%02d', $converted['hours'], $converted['minutes'], $converted['seconds']);
                if (trim($result) === '') {
                    $result = '0:00:00';
                }
                break;
        }
        return $result;
    }
}
