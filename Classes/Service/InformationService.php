<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Services for information
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class InformationService implements SingletonInterface
{
    /**
     * @param string $extensionKey
     * @return string
     */
    public static function formEngineInformation(string $extensionKey): string
    {
        //
        $result = '';
        $extensionName = str_replace('_', '-', $extensionKey);
        //
        // Check if we should note about a missing pro-version
        if (self::hasNonInstalledProVersion($extensionKey)) {
            $link = 'https://www.coding.ms/redirect.php?extensionKey=' . $extensionKey . '&t=pro';
            $result = LocalizationUtility::translate('tx_additionaltca_message.info_pro_version_of_extension', $extensionName);
            $result .= LocalizationUtility::translate('tx_additionaltca_message.info_more_information', 'AdditionalTca')
                . '<a href="' . $link . '" target="_blank">'
                . LocalizationUtility::translate('tx_additionaltca_message.info_here', 'AdditionalTca')
                . '</a>!';
        }
        if ($result !== '') {
            $closeTitle = 'Keine weiteren Vorschläge anzeigen!';
            $result = '<div class="alert alert-info alert-suggestion d-none mt-2" role="alert">'
                . $result
                . '<button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-label="' . $closeTitle . '" title="' . $closeTitle . '" data-dismiss-suggestion-button="1"></button>'
                . '</div>';
        }
        return $result;
    }

    /**
     * @param string $extensionKey
     * @return string
     */
    public static function drawItemInformation(string $extensionKey): string
    {
        //
        // Check if suggestion feature is disabled
        /** @var ExtensionConfiguration $extensionConfiguration */
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        $configuration = $extensionConfiguration->get('additional_tca');
        if (!empty($configuration)) {
            $active = $configuration['extension']['suggestions']['active'] ?? true;
            if (!$active) {
                return '';
            }
        }
        //
        $result = '';
        $extensionName = str_replace('_', '-', $extensionKey);
        //
        // Check if we should note about a missing pro-version
        if (self::hasNonInstalledProVersion($extensionKey)) {
            $link = 'https://www.coding.ms/redirect.php?extensionKey=' . $extensionKey . '&t=pro';
            $result = LocalizationUtility::translate('tx_additionaltca_message.info_pro_version_of_extension', $extensionName);
            $result .= LocalizationUtility::translate('tx_additionaltca_message.info_more_information', 'AdditionalTca')
                . '<a href="' . $link . '" target="_blank">'
                . LocalizationUtility::translate('tx_additionaltca_message.info_here', 'AdditionalTca')
                . '</a>!';
        }
        if ($result === '') {
            $result = self::otherExtension();
        }
        if ($result !== '') {
            $closeTitle = LocalizationUtility::translate('tx_additionaltca_message.info_no_more_suggestions', 'AdditionalTca');
            $result = '<div class="alert alert-info alert-suggestion d-none mb-0" role="alert">'
                . $result
                . '<button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-label="' . $closeTitle . '" title="' . $closeTitle . '" data-dismiss-suggestion-button="1"></button>'
                . '</div>';
        }
        return $result;
    }

    protected static function hasNonInstalledProVersion(string $extensionKey): bool
    {
        $hasNonInstalledProVersion = false;
        $proVersion = [
            'poll',
            'openimmo',
            'fahrzeugsuche',
            'portfolios',
            'bookings',
            'shop',
            'isotope', // test missing por-version
            'newsletters',
            'address_manager',
            'job_finder',
            'calendars',
            'downloadmanager',
            'adventskalender'
        ];
        if (in_array($extensionKey, $proVersion)
            && !ExtensionManagementUtility::isLoaded($extensionKey . '_pro')) {
            $hasNonInstalledProVersion = true;
        }
        return $hasNonInstalledProVersion;
    }

    protected static function hasNonInstalledAddOn(string $extensionKey): bool
    {
        $hasNonInstalledAddOn = false;
        $proVersion = [
            'openimmo' => ['immoscout'],
            'fahrzeugsuche' => ['autoscout', 'eautoseller'],
            'shop' => ['ups_api'],
        ];
        /**
         * @todo finalize implemetation
         */
        return $hasNonInstalledAddOn;
    }

    public static function getDocumentationLink(string $extensionKey): string
    {
        $styleFix = 'margin: -4px -4px;';
        $link = 'https://www.coding.ms/redirect.php?extensionKey=' . $extensionKey . '&t=doc';
        $result = '<span class="icon icon-size-small icon-state-default">
            <span class="icon-markup">
                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 16 16"><g fill="currentColor"><path d="M13.5 1h-11c-.271 0-.5.223-.5.498v13.004c0 .278.224.498.5.498h11c.271 0 .5-.223.5-.498V1.498A.497.497 0 0 0 13.5 1ZM3 2h1v12H3V2Zm2 0h8v12H5V2Zm1 1h6v1H6V3Z"/></g></svg>
            </span>
        </span>';
        return '<a href="' . $link . '" target="_blank" class="btn btn-sm btn-info float-end" style="' . $styleFix . '">'
            . $result . ' ' . LocalizationUtility::translate('tx_additionaltca_message.info_documentation', 'AdditionalTca') . '</a>';
    }

    public static function getDocumentationLinkFormEngine(string $extensionKey): string
    {
        $link = 'https://www.coding.ms/redirect.php?extensionKey=' . $extensionKey . '&t=doc';
        $result = '<span class="icon icon-size-small icon-state-default">
            <span class="icon-markup">
                <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" viewBox="0 0 16 16"><g fill="currentColor"><path d="M13.5 1h-11c-.271 0-.5.223-.5.498v13.004c0 .278.224.498.5.498h11c.271 0 .5-.223.5-.498V1.498A.497.497 0 0 0 13.5 1ZM3 2h1v12H3V2Zm2 0h8v12H5V2Zm1 1h6v1H6V3Z"/></g></svg>
            </span>
        </span>';
        return '<a href="' . $link . '" target="_blank" class="btn btn-sm btn-info" style="position: absolute; top: -30px; right: 0;">'
            . $result . ' ' . LocalizationUtility::translate('tx_additionaltca_message.info_documentation', 'AdditionalTca') . '</a>';
    }

    protected static function otherExtension(int $cycle = 0): string
    {
        $maxCycle = 50;
        $othersDE = [
            'openimmo' => 'Kennst Du schon die Openimmo-Erweiterung, mit der Immobilien einfach von Immoscout oder Flowfact uvm. auf deiner Website angeboten werden können?! Dann schau dir die Openimmo-Erweiterung an!',
            'shop' => 'Kennst Du schon die Shop-Erweiterung, mit der deiner Website einfach zum Online-Shop wird?! Dann schau dir die Shop-Erweiterung an!',
            //'ace_editor' => 'Du editierst of Script- oder Text-Dateien über den Fileadmin?! Dann könnte die ACE-Editor Erweiterung interessant sein für dich!',
            'address_manager' => 'Kennst musst verschiedene Standorte, Personen oder Filialen auf der Website darstellen?! Vielleicht sogar mit Umkreissuche? Dann schau dir die Address-Manager-Erweiterung an!',
            'adventskalender' => 'Du möchtest in der Weihnachtszeit deinen Besuchern mit einem Online-Adventskalender etwas besonderes bieten?! Benefits, Gewinnspiele oder einfach nur eine Aufmerksamkeit? Dann schau dir die Adventskalender-Erweiterung an!',
            'age_verification' => 'Du möchtest mit einer Alters-Abfrage nur erwachsene Besucher auf deine Website lassen?! Dann schau dir die Age-Verification-Erweiterung an!',
            'autoscout' => 'Du möchtest Fahrzeuge direkt von einem Autoscout-Account auf deine Website bringen?! Dann schau dir die Autoscout-Erweiterung an!',
            'bookings' => 'Du möchtest buchbare Objekte wie Ferienwohnungen, Wohnmobile oder vieles andere auf deiner Website anbieten und verbuchen?! Dann schau dir die Bookings-Erweiterung an!',
            'double_optin_download' => 'Du möchtest deinen Besuchern Downloads oder PDF via E-Mail-Download anbieten, und dabei deren Kontaktdaten sammeln?! Dann schau dir die Double-optin-download-Erweiterung an!',
            'eautoseller' => 'Du möchtest Fahrzeuge direkt von einem eAutoseller-Account auf deine Website bringen?! Dann schau dir die eAutoseller-Erweiterung an!',
            'fahrzeugsuche' => 'Du möchtest Fahrzeuge direkt von einem mobile.de-, autoscout- oder eAutoseller-Account auf deine Website bringen?! Dann schau dir die Fahrzeugsuche-Erweiterung an!',
            'firebase_push' /* Ext-Key ist ohne: _push */ => 'Du möchtest Benarichtigungen von z.B. neuen News auf die mobilien Geräte deiner Besucher schicken?! Dann schau dir die Firebase-Erweiterung an!',
            'fluid_form' => 'Du möchtest vordefinierte, versionierbare AJAX-Formulare für deine Website bereitstellen?! Dann schau dir die Fluid-Form-Erweiterung an!',
            'glossaries' => 'Kennst Du schon die Glossaries-Erweiterung, mit der einfach ein SEO optimiertes FAQ auf der Website bereitgestelt werden kann?! Dann schau dir die Glossaries-Erweiterung an!',
            'image_watermark' => 'Du möchtest die Bilder Deiner Website automatisiert mit einem Wasser-Zeichen ausgeben?! Dann schau dir die Image-Watermark-Erweiterung an!',
            'immoscout' => 'Du möchtest Immobilien direkt von einem Immoscout-Account auf deine Website bringen?! Dann schau dir die Immoscout-Erweiterung an!',
            'isotope' => 'Du möchtest Inhalte mit einem Isotope-Effekt/Filter auf deine Website bringen?! Dann schau dir die Isotope-Erweiterung an!',
            'job_finder' => 'Du möchtest SEO optimiert Jobs und Stellenanzeigen auf deine Website bringen?! Dann schau dir die Job-Finder-Erweiterung an!',
            //'logotech' => 'Du möchtest Gebrauchtmaschinen mit der Logotech-API auf deine Website bringen?!',
            'masonry' => 'Du möchtest Inhalte mit einem Masonry-Effekt/Filter auf deine Website bringen?! Dann schau dir die Masonry-Erweiterung an!',
            'newsletters' => 'Du möchtest Newsletter-Adressen via Double-Optin/Optout auf deiner Website sammeln?! Vielleicht sogar via Multi-Channel? Dann schau dir die Newsletters-Erweiterung an!',
            'points_of_interest' => 'Du möchtest Points-of-interest (wie bspw. Apotheken uvm.) auf einer Google-Maps auf deiner Website anzeigen?! Dann schau dir die Points-of-interest-Erweiterung an!',
            'poll' => 'Kennst Du schon die Poll-Erweiterung, mit der einfach Umfrage auf der Website gemacht werden können?! Dann schau dir die Poll-Erweiterung an!',
            'portfolios' => 'Du möchtest Referenzen oder ähnliche Datensätze mit umfangreichem Filter, Merkzettel, PDF-Generator, uvm. auf deiner Website anzeigen?! Dann schau dir die Portfolios-Erweiterung an!',
            'questions' => 'Kennst Du schon die Questions-Erweiterung, mit der einfach ein SEO optimiertes FAQ auf der Website bereitgestelt werden kann?! Dann schau dir die Questions-Erweiterung an!',
            'quiz' => 'Du möchest deinen Besucher ein Quiz anbieten oder sogar deine Mitarbeiter oder Bewerber schulen?! Dann schau dir die Quiz-Erweiterung an!',
            'rss_app' => 'Du möchest RSS-Feeds von andern Websites, Instagram oder vielen anderen Serivces und Portalen anzeigen?! Dann schau dir die RSS-App-Erweiterung an!',
            'schedulermonitor' => 'Du möchest benachrichtigt werden, wenn ein Scheduler-Task hängen geblieben ist?! Dann schau dir die Schedulermonitor-Erweiterung an!',
            'schemaorg' => 'Du möchest Microdaten/schema.org für deine Website einbinden?! Dann schau dir die Schemaorg-Erweiterung an!',
            'slick_modal' => 'Du möchest Inhalte oder ganze Seiten in einem Slick-Modal anzeigen?! Dann schau dir die Slick-Modal-Erweiterung an!',
            'view_statistics' => 'Du möchest die Klicks auf Seiten, News oder anderen Einzelansichten tracken ohne ein zusätzliches Tool wie Google-Analytics?! Dann schau dir die View-Statistics-Erweiterung an!',
        ];
        $othersEN = [
            'openimmo' => 'Have your heard about the Openimmo extension which can display real estate listings from Immoscout, Flowfact and others on your website? Check out the Openimmo extension!',
            'shop' => 'Have you heard about our shop extension which can easily turn your website into an online store? Check out the Shop extension!',
            //'ace_editor' => 'Do you often edit script or text files through the fileadmin? The ACE editor extension could be interesting for you!',
            'address_manager' => 'Do you need to display locations, people or branches on your website? And want to use radius search? Check out the Address Manager extension!',
            'adventskalender' => 'Do you want to offer your visitors something special this Christmas season? Our Adventscalendar can include perks, competitions and small treats. Check out the Adventskalender extension!',
            'age_verification' => 'Do you want to restrict your website to adult visitors using an age query? Check out our Age Verification extension!',
            'autoscout' => 'Do you want to transfer vehicles from an Autoscout account onto your website? Check out the Autoscout extension!',
            'bookings' => 'Do you want to list bookable objects such as holiday apartments, mobile homes and many more on your website? Check out the Bookings extension!',
            'double_optin_download' => 'Do you want to offer your visitors downloads and PDFs via email download, and collect their contact details? Check out the Double-optin-download extension!',
            'eautoseller' => 'Do you want to transfer vehicles from an eAutoseller account onto your website? Check out the eAutoseller extension!',
            'fahrzeugsuche' => 'Do you want to transfer vehicles from a mobile.de, autoscout or eAutoseller account onto your website? Check out the Fahrzeugsuche extension!',
            'firebase_push' /* Ext-Key is without: _push */ => 'Do you want to send notifications such as the latest news to your visitors\' mobile devices? Check out the Firebase extension!',
            'fluid_form' => 'Do you want to provide predefined, versionable AJAX forms for your website? Check out the Fluid-Form extension!',
            'glossaries' => 'Have you heard about the Glossaries extension which provides simple, SEO-optimized FAQs on websites? Check out the Glossaries extension!',
            'image_watermark' => 'Do you want to output images on your website with an automatically-generated watermark? Check out the Image Watermark extension!',
            'immoscout' => 'Do you want to transfer real estate from an Immoscout account onto your website? Check out the Immoscout extension!',
            'isotope' => 'Do you want to display content with an Isotope effect/filter on your website? Check out the Isotope extension!',
            'job_finder' => 'Do you want to display SEO optimized jobs and job advertisements on your website? Check out the Job-Finder extension!',
            //'logotech' => 'Do you want to bring used machines with the Logotech API onto your website?!',
            'masonry' => 'Do you want to display content with a Masonry effect/filter on your website? Check out the Masonry extension!',
            'newsletters' => 'You want to collect newsletter addresses via double opt-in/opt-out on your website? Perhaps even via multi-channel? Check out the newsletters extension!',
            'points_of_interest' => 'Do you want to display points of interest (such as pharmacies, etc) on a Google Map on your website? Check out the Points-of-interest extension!',
            'poll' => 'Have you heard about the Poll extension which can add surveys to your website? Check out the Poll extension!',
            'portfolios' => 'Do you want to display references or other data with filters, notepads, PDF generators and more on your website? Check out the Portfolios extension!',
            'questions' => 'Have you heard about the Questions extension, which can provide SEO-optimized FAQs on your website? Check out the Questions extension!',
            'quiz' => 'Do you want a quiz for your visitors or even to train employees or applicants? Then check out the Quiz extension!',
            'rss_app' => 'Do you want to display RSS feeds from other websites, Instagram or other services and portals? Then check out the RSS-App extension!',
            'schedulermonitor' => 'Do you want to be notified when a scheduler task is stuck? Then check out the Schedulermonitor extension!',
            'schemaorg' => 'Do you want to add microdata/schema.org to your website? Then check out the Schemaorg extension!',
            'slick_modal' => 'Do you want to display content or entire pages in a Slick Modal? Then check out the Slick-Modal extension!',
            'view_statistics' => 'Do you want to track clicks on pages, news or other elements without using an additional tool like Google-Analytics? Then check out the View-Statistics extension!',
        ];

        // Read links from website overview:
        // jQuery.each(jQuery('.card-isotope a'), function() {
        //     console.log(jQuery(this).attr('href'))
        // });

        // /de/typo3-extensions/typo3-openimmo
        // /de/typo3-extensions/typo3-shop
        // /de/typo3-extensions/typo3-ace-editor
        // /de/typo3-extensions/typo3-additional-tca
        // /de/typo3-extensions/typo3-address-manager
        // /de/typo3-extensions/typo3-adventskalender
        // /de/typo3-extensions/typo3-age-verification
        // /de/typo3-extensions/typo3-amp
        // /de/typo3-extensions/typo3-autoscout
        // /de/typo3-extensions/typo3-bookings
        // /de/typo3-extensions/typo3-commands
        // /de/typo3-extensions/typo3-double-optin-download
        // /de/typo3-extensions/typo3-downloadmanager
        // /de/typo3-extensions/typo3-eautoseller
        // /de/typo3-extensions/typo3-fahrzeugsuche
        // /de/typo3-extensions/typo3-firebase-push
        // /de/typo3-extensions/typo3-fluid-form
        // /de/typo3-extensions/typo3-fluid-fpdf
        // /de/typo3-extensions/typo3-geo-location
        // /de/typo3-extensions/typo3-glossaries
        // /de/typo3-extensions/typo3-image-watermark
        // /de/typo3-extensions/typo3-immoscout
        // /de/typo3-extensions/typo3-instagram
        // /de/typo3-extensions/typo3-internal-notes
        // /de/typo3-extensions/typo3-isotope
        // /de/typo3-extensions/typo3-job-finder
        // /de/typo3-extensions/typo3-logotech
        // /de/typo3-extensions/typo3-masonry
        // /de/typo3-extensions/typo3-modules
        // /de/typo3-extensions/typo3-newsletters
        // /de/typo3-extensions/typo3-parsedown-extra
        // /de/typo3-extensions/typo3-points-of-interest
        // /de/typo3-extensions/typo3-poll
        // /de/typo3-extensions/typo3-portfolios
        // /de/typo3-extensions/typo3-questions
        // /de/typo3-extensions/typo3-quiz
        // /de/typo3-extensions/typo3-rss-app
        // /de/typo3-extensions/typo3-schedulermonitor
        // /de/typo3-extensions/typo3-schemaorg
        // /de/typo3-extensions/typo3-slick-modal
        // /de/typo3-extensions/typo3-ups-api
        // /de/typo3-extensions/typo3-view-statistics
        $languageService =GeneralUtility::makeInstance(LanguageServiceFactory::class) ->createFromUserPreferences($GLOBALS['BE_USER']);
        if ($languageService->lang === 'de') {
            $others = $othersDE;
        } else {
            $others = $othersEN;
        }
        $extensionKey = array_rand($others);
        if (!ExtensionManagementUtility::isLoaded($extensionKey)) {
            $result = $others[$extensionKey];
            $link = 'https://www.coding.ms/redirect.php?extensionKey=' . $extensionKey . '&t=other';
            $result .= ' ' . LocalizationUtility::translate('tx_additionaltca_message.info_more_information', 'AdditionalTca')
                . ' <a href="' . $link . '" target="_blank"> '
                . LocalizationUtility::translate('tx_additionaltca_message.info_here', 'AdditionalTca')
                . '</a>!';
            return $result;
        }
        if ($cycle < $maxCycle) {
            return self::otherExtension(++$cycle);
        }
        return '';
    }
}
