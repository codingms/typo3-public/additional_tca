<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Service;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;

/**
 * Services for content types
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class ContentTypeService implements SingletonInterface
{
    /**
     * Converts the content-type into extension-key
     * @return string Extension key or empty string
     */
    public static function checkContentType(string $contentType): string
    {
        $result = '';
        if (str_starts_with($contentType, 'modules_')) {
            $result = 'modules';
        }
        if (str_starts_with($contentType, 'isotope_')) {
            $result = 'isotope';
        }
        if (str_starts_with($contentType, 'openimmo_')) {
            $result = 'openimmo';
        }
        if (str_starts_with($contentType, 'bookings_')) {
            $result = 'bookings';
        }
        if (str_starts_with($contentType, 'questions_')) {
            $result = 'questions';
        }
        if (str_starts_with($contentType, 'glossaries_')) {
            $result = 'glossaries';
        }
        if (str_starts_with($contentType, 'shop_')) {
            $result = 'shop';
        }
        if (str_starts_with($contentType, 'poll_')) {
            $result = 'poll';
        }
        if (str_starts_with($contentType, 'quiz_')) {
            $result = 'quiz';
        }
        if (str_starts_with($contentType, 'rssapp_')) {
            $result = 'rss_app';
        }
        if (str_starts_with($contentType, 'portfolios_')) {
            $result = 'portfolios';
        }
        if (str_starts_with($contentType, 'newsletters_')) {
            $result = 'newsletters';
        }
        if (str_starts_with($contentType, 'imagegallery_')) {
            $result = 'image_gallery';
        }
        if (str_starts_with($contentType, 'doubleoptindownload_')) {
            $result = 'double_optin_download';
        }
        if (str_starts_with($contentType, 'addressmanager_')) {
            $result = 'address_manager';
        }
        if (str_starts_with($contentType, 'jobfinder_')) {
            $result = 'job_finder';
        }
        if (str_starts_with($contentType, 'fluidform_')) {
            $result = 'fluid_form';
        }
        return $result;
    }
}
