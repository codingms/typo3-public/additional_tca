<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Resource\AbstractFile;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Configuration presets for TCA fields.
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class Configuration
{
    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label Additional label, e.g. checkboxes.
     * @param array<string, mixed> $options Options for select boxes, field name for slug
     * @return array<string, mixed>
     */
    public static function get(
        string $type,
        bool $required = false,
        bool $readonly = false,
        string $label = '',
        array $options = []
    ): array {
        $config = [];
        if ($type === 'frontend_user') {
            $type = 'frontendUserSingle';
            trigger_error(
                'Additional-TCA type frontend_user is deprecated, please use frontendUserSingle.',
                E_USER_DEPRECATED
            );
        }
        switch ($type) {
            case 'slug':
                $options['field'] = $options['field'] ?? 'title';
                $config = [
                    'type' => 'slug',
                    'size' => 50,
                    'prependSlash' => true,
                    'generatorOptions' => [
                        'fields' => [$options['field']],
                        'prefixParentPageSlug' => true,
                        'replacements' => [
                            '/' => '-'
                        ],
                    ],
                    'fallbackCharacter' => '-',
                    'eval' => 'lower,unique',
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'rte':
                $config = [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 15,
                    'eval' => 'trim',
                    'enableRichtext' => 1,
                    'richtextConfiguration' => $options['configuration'] ?? 'default',
                    'default' => '',
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'textareaSmall':
                $config = [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 5,
                    'eval' => 'trim',
                    'default' => '',
                    'fixedFont' => $options['fixedFont'] ?? false,
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'textareaLarge':
                $config = [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => 15,
                    'eval' => 'trim',
                    'default' => '',
                    'fixedFont' => $options['fixedFont'] ?? false,
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'expandableTextarea':
                $options['expandedInitially'] = isset($options['expandedInitially']) && $options['expandedInitially'];
                $options['size'] = $options['size'] ?? 'large';
                $config = [
                    'type' => 'text',
                    'cols' => 40,
                    'rows' => $options['size'] === 'small' ? 5 : 15,
                    'eval' => 'trim',
                    'renderType' => 'ExpandableTextarea',
                    'default' => '',
                    'fixedFont' => $options['fixedFont'] ?? false,
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'ExpandableTextarea',
                            'expandedInitially' => $options['expandedInitially'],
                        ],
                    ]
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'markdown':
                $config = [
                    'type' => 'text',
                    'renderType' => 'codeEditor',
                    'format' => 'markdown',
                    'cols' => 80,
                    'rows' => 60,
                    'eval' => 'trim',
                    'default' => '',
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 13) {
                    $config['renderType'] = 't3editor';
                }
                break;
            case 'html':
                $config = [
                    'type' => 'text',
                    'renderType' => 'codeEditor',
                    'format' => 'html',
                    'cols' => 80,
                    'rows' => 60,
                    'eval' => 'trim',
                    'default' => '',
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 13) {
                    $config['renderType'] = 't3editor';
                }
                break;
            case 'css':
                $config = [
                    'type' => 'text',
                    'renderType' => 't3editor',
                    'format' => 'css',
                    'cols' => 80,
                    'rows' => 60,
                    'eval' => 'trim',
                    'default' => '',
                ];
                break;
            case 'float':
                $config = [
                    'type' => 'number',
                    'size' => $options['size'] ?? 4,
                    'eval' => '',
                ];
                $config = self::insertRequired($config, $required);
                if (isset($options['default'])) {
                    $config['default'] = $options['default'];
                }
                break;
            case 'int':
                $config = [
                    'type' => 'number',
                    'size' => $options['size'] ?? 4,
                    'eval' => '',
                ];
                if (isset($options['lower']) && isset($options['upper'])) {
                    $config['range'] = [
                        'lower' => $options['lower'],
                        'upper' => $options['upper'],
                    ];
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'currency':
                $config = [
                    'type' => 'input',
                    'renderType' => 'Currency',
                    'size' => 10,
                    'eval' => 'int',
                    'default' => '0',
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Currency',
                        ],
                    ],
                    'dbType' => $options['dbType'] ?? null,
                ];
                // Save as float number?
                if (isset($options['dbType']) && $options['dbType'] === 'float') {
                    $config['eval'] = 'double2';
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'percent':
                $config = [
                    'type' => 'input',
                    'renderType' => 'Percent',
                    'size' => 4,
                    'eval' => 'int',
                    'default' => 0,
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Percent',
                        ],
                    ],
                    'dbType' => $options['dbType'] ?? null,
                ];
                // Save as float number?
                if (isset($options['dbType']) && $options['dbType'] === 'float') {
                    $config['eval'] = 'double2';
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'weight':
                $config = [
                    'type' => 'input',
                    'renderType' => 'Weight',
                    'size' => 4,
                    'eval' => 'int',
                    'default' => 0,
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Weight',
                        ],
                    ],
                    'dbType' => $options['dbType'] ?? null,
                ];
                // Save as float number?
                if (isset($options['dbType']) && $options['dbType'] === 'float') {
                    $config['eval'] = 'double2';
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'string':
                $config = [
                    'type' => 'input',
                    'size' => $options['size'] ?? 30,
                    'eval' => 'trim',
                    'default' => $options['default'] ?? '',
                ];
                $config = self::insertRequired($config, $required);
                if (isset($options['maxLength']) && (int)$options['maxLength'] > 0) {
                    $config['max'] = (int)$options['maxLength'];
                }
                break;
            case 'email':
                $config = [
                    'type' => 'email',
                    'size' => 30,
                    'eval' => 'trim',
                    'fieldInformation' => [
                        'EmailValidation' => [
                            'renderType' => 'Email',
                        ],
                    ],
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'color':
                $config = [
                    'type' => 'color',
                    'size' => $options['size'] ?? 10,
                    'eval' => ''
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'checkbox':
                $config = [
                    'type' => 'check',
                    'items' => [
                        ['label' => $label],
                    ],
                    'default' => $options['default'] ?? 0,
                ];
                break;
            case 'link':
                $availableTypes = ['page', 'url', 'record', 'mail', 'folder', 'file', 'telephone', 'spec'];
                $availableFields = ['class', 'target', 'title', 'params'];
                //
                // Transfer 'blindLinkOptions' -> flip the values!
                //
                // Source: 'blindLinkOptions' => 'file,folder,mail,telephone,spec'
                // Target: 'allowedTypes' => ['page', 'url', 'record'],
                if (isset($options['blindLinkOptions'])) {
                    $blindLinkOptions = GeneralUtility::trimExplode(
                        ',',
                        $options['blindLinkOptions'],
                        true
                    );
                    $options['allowedTypes'] = array_diff($availableTypes, $blindLinkOptions);
                    trigger_error(
                        'Additional-TCA uses type:link option:blindLinkOptions which is deprecated.',
                        E_USER_DEPRECATED
                    );
                }
                //
                // Transfer 'blindLinkFields' -> flip the values!
                //
                // Source: 'blindLinkFields' => 'params'
                // Target: 'allowedOptions' => ['class', 'target', 'title'],
                if (isset($options['blindLinkFields'])) {
                    $blindLinkOptions = GeneralUtility::trimExplode(
                        ',',
                        $options['blindLinkFields'],
                        true
                    );
                    $options['allowedOptions'] = array_diff($availableFields, $blindLinkOptions);
                    trigger_error(
                        'Additional-TCA uses type:link option:blindLinkFields which is deprecated.',
                        E_USER_DEPRECATED
                    );
                }
                //
                // Transfer title if old option usage
                if (isset($options['title'])) {
                    $options['browserTitle'] = $options['title'];
                    trigger_error(
                        'Additional-TCA uses type:link option:title which is deprecated.',
                        E_USER_DEPRECATED
                    );
                }
                $config = [
                    'type' => 'link',
                    'size' => 20,
                    'allowedTypes' => $options['allowedTypes'] ?? ['page', 'url', 'record', 'email', 'folder', 'file', 'telephone'],
                    'appearance' => [
                        'enableBrowser' => true,
                        'browserTitle' => $options['browserTitle'] ?? 'LLL:EXT:core/Resources/Private/Language/locallang_tca.xlf:sys_file_reference.link',
                        'allowedFileExtensions' => ['jpg', 'png'],
                        'allowedOptions' => $options['allowedOptions'] ?? ['class', 'target', 'title'],
                    ],
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'date':
                $options['dbType'] = $options['dbType'] ?? 'timestamp';
                $config = [
                    'dbType' => 'date',
                    'type' => 'datetime',
                    'format' => 'date',
                    'size' => $options['size'] ?? 12,
                    'eval' => '',
                    'default' => null,
                ];
                //
                // Add toolbar?
                if ($options['toolbar'] ?? false) {
                    $config['size'] = $options['size'] ?? 17;
                    $config['fieldInformation'] = [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Date',
                        ],
                    ];
                }
                // Save as timestamp?
                if ($options['dbType'] === 'timestamp') {
                    unset($config['dbType']);
                    $config['eval'] .= ',int';
                    $config['default'] = 0;
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'dateTime':
                $options['dbType'] = $options['dbType'] ?? 'timestamp';
                $config = [
                    'dbType' => 'datetime',
                    'type' => 'datetime',
                    'format' => 'datetime',
                    'size' => $options['size'] ?? 12,
                    'eval' => '',
                    'default' => null,
                ];
                //
                // Add toolbar?
                if ($options['toolbar'] ?? false) {
                    $config['size'] = $options['size'] ?? 17;
                    $config['fieldInformation'] = [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'DateTime',
                            'step' => intval($options['step'] ?? 15),
                            'toolbar' => true,
                        ],
                    ];
                }
                // Save as timestamp?
                if ($options['dbType'] === 'timestamp') {
                    unset($config['dbType']);
                    $config['eval'] .= ',int';
                    $config['default'] = 0;
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'duration':
                $config = [
                    'type' => 'input',
                    'renderType' => 'Duration',
                    'size' => $options['size'] ?? 10,
                    'eval' => 'int',
                    'default' => $options['default'] ?? '0',
                ];
                //
                // Add toolbar?
                if ($options['toolbar'] ?? false) {
                    $config['size'] = $options['size'] ?? 10;
                    $config['fieldInformation'] = [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'DateTime',
                            'step' => intval($options['step'] ?? 15),
                            'toolbar' => true,
                        ],
                    ];
                }
                $config = self::insertRequired($config, $required);
                break;
            case 'select':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'eval' => 'trim',
                    'items' => $options,
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'fileSingle':
                $config = [
                    'type' => 'file',
                    'allowed' => 'md',
                    'size' => '1',
                    'maxitems' => '1',
                    'minitems' => $required ? '1' : '0',
                    'eval' => '',
                    'show_thumbs' => '1',
                    'wizards' => [
                        'suggest' => [
                            'type' => 'suggest',
                        ],
                    ],
                ];
                break;
            case 'fileCollectionSingleInline':
                $config = [
                    'type' => 'inline',
                    'foreign_table' => 'sys_file_collection',
                    'minitems' => 0,
                    'maxitems' => 1,
                    'appearance' => [
                        'collapseAll' => 0,
                        'levelLinksPosition' => 'top',
                        'showSynchronizationLink' => 1,
                        'showPossibleLocalizationRecords' => 1,
                        'showAllLocalizationLink' => 1
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'imageSingleAltTitle':
                $fileTypes = 'png,jpg,jpeg';
                if (isset($options['fileTypes'])) {
                    $fileTypes = $options['fileTypes'];
                }
                $config = [
                    'type' => 'file',
                    'allowed' => $fileTypes,
                    'maxitems' => 1,
                    'default' => 0,
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => 'title,alternative,
                               --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_IMAGE => [
                                'showitem' => 'title,alternative,
                               --palette--;;filePalette',
                            ],
                        ],
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'imageSingleTitleDescription':
                $field = $options['field'] ?? 'image';
                $fileTypes = 'png,jpg,jpeg';
                if (isset($options['fileTypes'])) {
                    $fileTypes = $options['fileTypes'];
                }
                $config = [
                    'type' => 'file',
                    'allowed' => $fileTypes,
                    'maxitems' => 1,
                    'default' => 0,
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => 'title,alternative,
                                --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_IMAGE => [
                                'showitem' => 'title,alternative,
                                --palette--;;filePalette',
                            ],
                        ],
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'images':
                $config = [
                    'type' => 'file',
                    'allowed' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // to use the imageoverlayPalette instead of the basicoverlayPalette
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_TEXT => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_IMAGE => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_AUDIO => [
                                'showitem' => '
                                --palette--;;audioOverlayPalette,
                                --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_VIDEO => [
                                'showitem' => '
                                --palette--;;videoOverlayPalette,
                                --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_APPLICATION => [
                                'showitem' => '
                                --palette--;;imageoverlayPalette,
                                --palette--;;filePalette',
                            ]
                        ],
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'imagesAltTitle':
                $fileTypes = 'png,jpg,jpeg';
                if (isset($options['fileTypes'])) {
                    $fileTypes = $options['fileTypes'];
                }
                $config = [
                    'type' => 'file',
                    'allowed' => $fileTypes,
                    'maxitems'=> 99,
                    'default' => 0,
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => 'title,alternative,
                                --palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_IMAGE => [
                                'showitem' => 'title,alternative,
                                --palette--;;filePalette',
                            ],
                        ]
                    ]
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'coordinate':
                $config = [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'double8',
                    'default' => 0.0,
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'videoSingle':
                $fileTypes = 'mp4';
                if (isset($options['fileTypes'])) {
                    $fileTypes = $options['fileTypes'];
                }
                $config = [
                    'type' => 'file',
                    'allowed' => $fileTypes,
                    'maxitems' => 1,
                    'overrideChildTca' => [
                        'types' => [
                            '4' => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                            AbstractFile::FILETYPE_IMAGE => [
                                'showitem' => '--palette--;;filePalette',
                            ],
                        ],
                    ],
                ];
                if ($required) {
                    $config['minitems'] = 1;
                }
                break;
            case 'frontendUserSingle':
                $config = [
                    'type' => 'group',
                    'allowed' => 'fe_users',
                    'foreign_table' => 'fe_users',
                    'size' => '1',
                    'maxitems' => '1',
                    'minitems' => '0',
                    'eval' => 'int',
                    'default' => 0,
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'badgeSuggested':
                $config = [
                    'type' => 'input',
                    'size' => 30,
                    'eval' => 'trim',
                    'renderType' => 'BadgeSuggested',
                    'default' => '',
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'BadgeSuggested',
                        ],
                    ],
                ];
                $config = self::insertRequired($config, $required);
                break;
            case 'notice':
                $config = [
                    'renderType' => 'Notice',
                    // Don't use a default value
                    // Otherwise the FormEngine tries to persist them!
                    // 'default' => '',
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Notice',
                        ],
                    ],
                ];
                $config['type'] = $options['type'] ?? 'input';
                $config['display'] = $options['display'] ?? 'none';
                $config['html'] = $options['html'] ?? '';
                $config['notice'] = $options['notice'] ?? '';
                break;
            case 'phone':
                $config = [
                    'type' => 'input',
                    'renderType' => 'Phone',
                    'size' => $options['size'] ?? 12,
                    'eval' => 'trim',
                    'default' => $options['default'] ?? '',
                    'fieldInformation' => [
                        'AdditionalInfoPageTitle' => [
                            'renderType' => 'Phone',
                        ],
                    ],
                    'country' => $options['country'] ?? 'DE',
                ];
                $config = self::insertRequired($config, $required);
                if (isset($options['maxLength']) && (int)$options['maxLength'] > 0) {
                    $config['max'] = (int)$options['maxLength'];
                }
                break;
        }
        $config['readOnly'] = $readonly;
        if (isset($options['default'])) {
            $config['default'] = $options['default'];
        }
        return $config;
    }

    /**
     * @param string $type
     * @param string $table
     * @param string $extensionKey
     * @return array<string, mixed>
     *
     * @example
     * 'columns' => [
     *     'sys_language_uid' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('sys_language_uid'),
     *     'l10n_parent' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('l10n_parent', $table, $extKey),
     *     'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('l10n_diffsource'),
     *     't3ver_label' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('t3ver_label'),
     *     'hidden' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('hidden'),
     *     'starttime' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('starttime'),
     *     'endtime' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('endtime'),
     *     'support' => \CodingMs\AdditionalTca\Tca\ConfigurationPresetCustom::full('support', $table, $extKey),
     */
    public static function full(string $type, string $table='', string $extensionKey=''): array
    {
        $config = [];
        switch ($type) {
            case 'sys_language_uid':
                $config = [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
                    'config' => [
                        'type' => 'language',
                    ],
                ];
                break;
            case 'l10n_parent':
                $config = [
                    'displayCond' => 'FIELD:sys_language_uid:>:0',
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            [
                                'label' => '',
                                'value' => 0,
                            ],
                        ],
                        'foreign_table' => $table,
                        // no sys_language_uid = -1 allowed explicitly!
                        'foreign_table_where' => 'AND {#' . $table . '}.{#uid}=###CURRENT_PID### AND {#' . $table . '}.{#sys_language_uid} = 0',
                        'default' => 0,
                    ],
                ];
                break;
            case 'l10n_diffsource':
                $config = [
                    'config' => [
                        'type' => 'passthrough',
                    ],
                ];
                break;
            case 't3ver_label':
                $config = [
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
                    'config' => [
                        'type' => 'input',
                        'size' => 30,
                        'max' => 255,
                    ]
                ];
                break;
            case 'hidden':
                $config = [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
                    'config' => [
                        'type' => 'check',
                    ],
                ];
                break;
            case 'starttime':
                $config = [
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
                    'config' => [
                        'type' => 'datetime',
                        'default' => 0,
                    ]
                ];
                break;
            case 'endtime':
                $config = [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
                    'config' => [
                        'type' => 'datetime',
                        'default' => 0,
                    ],
                ];
                break;
            case 'information':
                $config = [
                    'exclude' => 0,
                    'label' => '',
                    'config' => [
                        'type' => 'user',
                        'renderType' => 'Information',
                        'userFunc' => 'CodingMs\\AdditionalTca\\Tca\\InformationRow->renderField',
                        'parameters' => [
                            'extensionKey' => $extensionKey,
                        ],
                    ]
                ];
                break;
        }
        return $config;
    }

    /**
     * @param string $type
     * @return string
     */
    public static function label(string $type): string
    {
        $label = [
            'tab_general' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general',
            'tab_language' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language',
            'tab_access' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access',
            'tab_notes' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes',
            'tab_extended' => 'LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended',
            'tab_seo' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_seo',
            'tab_relations' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_relations',
            'tab_images' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_images',
            'tab_files' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_files',
            'tab_links' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_links',
            'tab_markdown' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_markdown',
            'tab_contact' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_contact',
            'tab_persons' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_persons',
            'tab_bookings' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_bookings',
            'tab_prices' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_prices',
            'tab_map' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_map',
            'tab_registration' => 'LLL:EXT:additional_tca/Resources/Private/Language/locallang.xlf:tx_additionaltca_label.tab_registration',
        ];
        return $label[$type] ?? $type;
    }

    /**
     * @param array<string, mixed> $config
     * @param bool $required
     * @return array<string, mixed>
     */
    protected static function insertRequired(array $config, bool $required): array
    {
        $config['required'] = $required;
        return $config;
    }
}
