<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Hooks;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2021 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Add some JavaScript and Stylesheets to backend
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class PageRendererHook
{
    /**
     * @param array<string, string> $parameters An array of available parameters
     */
    public function addJavaScriptAndStylesheets(array $parameters = []): void
    {
        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        //
        // Add JavaScript translation value
        $translations = [
            'unit_day' => 'tx_additionaltca_label.unit_day',
            'unit_minutes' => 'tx_additionaltca_label.unit_minutes',
            'select_current_day' => 'tx_additionaltca_label.select_current_day',
            'select_current_day_time' => 'tx_additionaltca_label.select_current_day_time',
        ];
        if ($pageRenderer->getApplicationType() === 'BE') {
            foreach ($translations as $javaScriptKey => $translationKey) {
                $pageRenderer->addInlineLanguageLabel(
                    $javaScriptKey,
                    LocalizationUtility::translate($translationKey, 'AdditionalTca') ?? ''
                );
            }
            $pageRenderer->getJavaScriptRenderer()->addJavaScriptModuleInstruction(
                JavaScriptModuleInstruction::create('@codingms/additional-tca/Backend/DismissSuggestions.js')
                    ->invoke('initialize')
            );
        }
    }
}
