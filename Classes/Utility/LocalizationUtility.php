<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2021 Christian Bülter <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility as LocalizationUtilityCore;

/**
 * Localization Tools
 */
class LocalizationUtility
{
    /**
     * @param string $key
     * @param array<int, string> $arguments
     * @param bool $preventException Prevent throwing an exception when translation is not available
     * @return string
     * @throws Exception
     */
    public static function translate(string $key, array $arguments = null, bool $preventException = false): string
    {
        $translation = LocalizationUtilityCore::translate($key, 'AdditionalTca', $arguments);
        if ($translation === null && !$preventException) {
            $path = 'EXT:additional_tca/Resources/Private/Language/locallang.xlf';
            throw new Exception('Translation ' . $key . ' not found in ' . $path);
        }
        if ($translation === null) {
            $translation = $key;
        }
        return $translation;
    }
}
