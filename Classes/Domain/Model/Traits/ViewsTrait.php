<?php

namespace CodingMs\AdditionalTca\Domain\Model\Traits;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

trait ViewsTrait
{
    protected int $views = 0;
    protected int $viewsDay = 0;
    protected int $viewsWeek = 0;
    protected int $viewsMonth = 0;
    protected int $viewsYear = 0;

    public function getViews(): int
    {
        return $this->views;
    }

    public function increaseViews(): void
    {
        $this->views += 1;
    }

    public function getViewsDay(): int
    {
        return $this->viewsDay;
    }

    public function getViewsWeek(): int
    {
        return $this->viewsWeek;
    }

    public function getViewsMonth(): int
    {
        return $this->viewsMonth;
    }

    public function getViewsYear(): int
    {
        return $this->viewsYear;
    }
}
