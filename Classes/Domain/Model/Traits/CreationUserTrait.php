<?php

namespace CodingMs\AdditionalTca\Domain\Model\Traits;

use TYPO3\CMS\Beuser\Domain\Model\BackendUser;
use TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

trait CreationUserTrait
{
    /**
     * !!! ATTENTION !!!
     * We don't set a type on the property,
     * because otherwise we get a conflict with lazy loading,
     * because union-types are not supported yet!
     *
     * @var ?BackendUser
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $creationUser;

    public function getCreationUser(): ?BackendUser
    {
        /** @phpstan-ignore-next-line */
        if ($this->creationUser instanceof LazyLoadingProxy) {
            $this->creationUser->_loadRealInstance();
        }
        return $this->creationUser;
    }

    public function setCreationUser(BackendUser $creationUser): void
    {
        $this->creationUser = $creationUser;
    }
}
