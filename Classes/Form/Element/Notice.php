<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Form\Element;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Form\Element\Traits\Typo3Version13Trait;
use TYPO3\CMS\Backend\Form\AbstractNode;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class Notice extends AbstractNode
{
    use Typo3Version13Trait;

    /**
     * Handler for single nodes
     *
     * @return array<mixed> As defined in initializeResultArray() of AbstractNode
     */
    public function render(): array
    {
        $result = $this->initializeResultArray();
        $fieldName = $this->data['fieldName'];
        //
        // Identify the notice display
        $display = 'none';
        $displayClasses = '';
        $styles = '';
        if (isset($this->data['parameterArray']['fieldConf']['config']['display'])) {
            $display = trim($this->data['parameterArray']['fieldConf']['config']['display']);
        }
        if ($display !== '' && $display !== 'none' && $display !== 'code') {
            $displayClasses = 'alert alert-' . $display;
            $styles .= 'margin-bottom: 0;';
        } elseif ($display === 'none') {
            $displayClasses = 'text-muted';
        }
        //
        // Identify the notice content
        $notice = 'No content! Maybe your TCA needs to be migrated from key "information" to "notice"!? More information on: https://gitlab.com/codingms/typo3-public/additional_tca/-/blob/master/Migration.md';
        $html = '';
        $htmlContentAvailable = false;
        if ($this->isTypo3Version13OrNewer()) {
            $html = $this->generateInputLabelHtml([], $fieldName);
        }
        if (isset($this->data['parameterArray']['fieldConf']['config']['html'])
            && trim($this->data['parameterArray']['fieldConf']['config']['html']) !== '') {
            $html = trim($this->data['parameterArray']['fieldConf']['config']['html']);
            $htmlContentAvailable = true;
        }
        if ($html !== '' && $htmlContentAvailable) {
            $notice = $html;
        } elseif (isset($this->data['parameterArray']['fieldConf']['config']['notice'])) {
            $notice = trim((string)$this->data['parameterArray']['fieldConf']['config']['notice']);
            //
            // notice is a translation value
            if (str_starts_with($notice, 'LLL:')) {
                $notice = nl2br((string)LocalizationUtility::translate($notice));
            }
        }
        if ($display === 'code') {
            $notice = '<div class="font-monospace" style="border: 1px solid #d1d1d1; background-color: #fff; padding: 10px">' . $notice . '</div>';
        }
        $result['html'] = '<div class="formengine-field-item t3js-formengine-field-item">
            <div class="form-wizards-wrap ' . $displayClasses . '" style="' . $styles . '">
                <div class="form-wizards-element">' . $notice . '</div>
            </div>
        </div>';
        return $result;
    }
}
