<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Form\Element;

use CodingMs\AdditionalTca\Form\Element\Traits\Typo3Version13Trait;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;

class Percent extends AbstractFormElement
{
    use Typo3Version13Trait;

    /**
     * Handler for single nodes
     *
     * @return array<mixed> As defined in initializeResultArray() of AbstractNode
     */
    public function render(): array
    {
        $languageService = $this->getLanguageService();
        $table = $this->data['tableName'];
        $fieldName = $this->data['fieldName'];
        $row = $this->data['databaseRow'];
        $parameterArray = $this->data['parameterArray'];
        $resultArray = $this->initializeResultArray();
        $config = $parameterArray['fieldConf']['config'];
        $size = MathUtility::forceIntegerInRange($config['size'] ?: $this->defaultInputWidth, $this->minimumInputWidth, $this->maxInputWidth);
        $width = (int)$this->formMaxWidth($size);
        if ($config['dbType'] === 'float') {
            $itemValue = (float)$parameterArray['itemFormElValue'];
            $itemValue = number_format($itemValue, 2, ',', '.') . ' %';
        } else {
            $itemValue = (int)$parameterArray['itemFormElValue'];
            $itemValue = number_format($itemValue / 100, 2, ',', '.') . ' %';
        }
        $id = StringUtility::getUniqueId('formengine-input-');
        $attributes = [
            'value' => $itemValue,
            'id' => $id,
            'class' => implode(' ', [
                'form-control',
                'hasDefaultValue',
                't3js-clearable',
                't3js-percent',
                'formengine-percentelement',
            ]),
            'data-formengine-validation-rules' => $this->getValidationDataAsJsonString([]), //$config),
            'data-formengine-input-params' => (string)json_encode([
                'field' => '',
                'evalList' => '',
                'is_in' => '',
            ]),
            'data-formengine-input-name' => $parameterArray['itemFormElName'],
            'data-db-type' => ($config['dbType'] === 'float') ? 'float' : 'int',
        ];
        if ($this->isTypo3Version13OrNewer()) {
            unset($attributes['data-formengine-input-params']);
        }
        //
        // Is read only?!
        if (isset($config['readOnly']) && $config['readOnly']) {
            $attributes['readonly'] = 'readonly';
            $attributes['disabled'] = 'disabled';
            $attributes['class'] = str_replace('t3js-clearable', '', $attributes['class']);
        }
        //
        // Load needed js library
        $resultArray['javaScriptModules'][] = JavaScriptModuleInstruction::create(
            '@codingms/additional-tca/Backend/FormEngine/Element/PercentElement.js'
        )->invoke('initialize', $id);
        //
        // HTML
        $html = [];
        if ($this->isTypo3Version13OrNewer()) {
            $html[] = $this->generateInputLabelHtml($parameterArray, $fieldName);
        }
        $html[] = '<div class="formengine-field-item t3js-formengine-field-item">';
        if (isset($parameterArray['fieldConf']['description'])) {
            $description = $parameterArray['fieldConf']['description'];
            $html[] =   '<span class="formengine-field-item-description text-muted">' . $description . '</span>';
        }
        $html[] =   '<div class="form-wizards-wrap">';
        $html[] =       '<div class="form-wizards-element">';
        $html[] =           '<div class="form-control-wrap" style="max-width: ' . $width . 'px">';
        $html[] =          '<input type="text"' . GeneralUtility::implodeAttributes($attributes, true) . ' />';
        $html[] =          '<input type="hidden" readonly="readonly" name="' . $parameterArray['itemFormElName'] . '" value="' . htmlspecialchars($itemValue) . '" id="' . $id . '_hidden" />';
        $html[] =       '</div>';
        $html[] =       '</div>';
        $html[] =   '</div>';
        $html[] = '</div>';
        $resultArray['html'] = implode(PHP_EOL, $html);
        return $resultArray;
    }
}
