<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Form\Element;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2022 Roman Derlemenko <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Form\Element\Traits\OnFieldChangeTrait;
use CodingMs\AdditionalTca\Form\Element\Traits\Typo3Version13Trait;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;

/**
 * ExpandableTextarea
 */
class ExpandableTextarea extends AbstractFormElement
{
    use OnFieldChangeTrait;
    use Typo3Version13Trait;

    /**
     * The number of chars expected per row when the height of a text area field is
     * automatically calculated based on the number of characters found in the field content.
     *
     * @var int
     */
    protected $charactersPerRow = 40;

    /**
     * Handler for single nodes
     *
     * @return array<mixed> As defined in initializeResultArray() of AbstractNode
     */
    public function render(): array
    {
        $parameterArray = $this->data['parameterArray'];
        $fieldName = $this->data['fieldName'];
        $resultArray = $this->initializeResultArray();
        //
        // Insert field wizards
        $fieldWizardResult = $this->renderFieldWizard();
        $fieldWizardHtml = $fieldWizardResult['html'];
        $resultArray = $this->mergeChildReturnIntoExistingResult($resultArray, $fieldWizardResult, false);
        //
        $itemValue = $parameterArray['itemFormElValue'];
        $config = $parameterArray['fieldConf']['config'];
        $evalList = GeneralUtility::trimExplode(',', $config['eval'] ?? '', true);
        $width = null;
        if ($config['cols'] ?? false) {
            $width = $this->formMaxWidth(MathUtility::forceIntegerInRange($config['cols'], $this->minimumInputWidth, $this->maxInputWidth));
        }

        // Setting number of rows
        $rows = MathUtility::forceIntegerInRange(($config['rows'] ?? 5) ?: 5, 1, 20);
        $originalRows = $rows;
        $itemFormElementValueLength = strlen((string)$itemValue);
        if ($itemFormElementValueLength > $this->charactersPerRow * 2) {
            $rows = MathUtility::forceIntegerInRange(
                (int)round($itemFormElementValueLength / $this->charactersPerRow),
                count(explode(PHP_EOL, $itemValue)),
                20
            );
            if ($rows < $originalRows) {
                $rows = $originalRows;
            }
        }

        $id = StringUtility::getUniqueId('formengine-input-');
        $attributes = [
            'value' => $itemValue,
            'id' => $id,
            'rows' => (string)$rows,
            'wrap' => (string)(($config['wrap'] ?? 'virtual') ?: 'virtual'),
            'class' => implode(' ', [
                'form-control',
                't3js-formengine-textarea',
                'formengine-textarea',
                'expandable-text',
            ]),
            'data-formengine-validation-rules' => $this->getValidationDataAsJsonString($config),
            'data-formengine-input-name' => $parameterArray['itemFormElName'],
        ];
        //
        // Is read only?!
        if ($config['readOnly'] ?? false) {
            $attributes['readonly'] = 'readonly';
            $attributes['disabled'] = 'disabled';
        }
        //
        // Load needed js library
        $resultArray['javaScriptModules'][] = JavaScriptModuleInstruction::create(
            '@codingms/additional-tca/Backend/FormEngine/Element/ExpandableTextareaElement.js'
        )->invoke('initialize', $id, $this->getExpandedInitially());
        //
        // HTML
        $initiallyHidden = $this->getExpandedInitially() ? '' : ' style="display:none"';
        $html = [];
        if ($this->isTypo3Version13OrNewer()) {
            $html[] = $this->generateInputLabelHtml($parameterArray, $fieldName);
        }
        $html[] = '<div class="formengine-field-item t3js-formengine-field-item" ' . $initiallyHidden . '>';
        $html[] =   '<div class="form-control-wrap" style="margin-bottom: -1px;">';
        $html[] =     '<div class="form-wizards-wrap">';
        $html[] =       '<div class="form-wizards-element">';
        $html[] =            '<textarea ' . GeneralUtility::implodeAttributes($attributes, true) . ' name="' . $parameterArray['itemFormElName'] . '" id="' . $id . '">' . htmlspecialchars((string)$itemValue) . '</textarea>';
        $html[] =       '</div>';
        if (!empty($fieldWizardHtml)) {
            $html[] = '<div class="form-wizards-items-bottom">';
            $html[] = $fieldWizardHtml;
            $html[] = '</div>';
        }
        $html[] =     '</div>';
        $html[] =   '</div>';
        $html[] = '</div>';
        $resultArray['html'] = implode(PHP_EOL, $html);

        return $resultArray;
    }

    /**
     * @return bool
     */
    protected function getExpandedInitially(): bool
    {
        return boolval($this->data['processedTca']['columns'][$this->data['fieldName']]['config']['fieldInformation']['AdditionalInfoPageTitle']['expandedInitially'] ?? false);
    }
}
