<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Form\Element;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Form\Element\Traits\Typo3Version13Trait;
use CodingMs\AdditionalTca\Service\FormatService;
use CodingMs\AdditionalTca\Utility\LocalizationUtility;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;

class Duration extends AbstractFormElement
{
    use Typo3Version13Trait;

    /**
     * Handler for single nodes
     *
     * @return array<mixed> As defined in initializeResultArray() of AbstractNode
     * @throws \TYPO3\CMS\Core\Exception
     */
    public function render(): array
    {
        $parameterArray = $this->data['parameterArray'];
        $fieldName = $this->data['fieldName'];
        $resultArray = $this->initializeResultArray();
        //
        // Insert field wizards
        $fieldWizardResult = $this->renderFieldWizard();
        $fieldWizardHtml = $fieldWizardResult['html'];
        $resultArray = $this->mergeChildReturnIntoExistingResult($resultArray, $fieldWizardResult, false);
        //
        $itemValue = (int)$parameterArray['itemFormElValue'];
        $config = $parameterArray['fieldConf']['config'];
        $size = MathUtility::forceIntegerInRange($config['size'] ?: $this->defaultInputWidth, $this->minimumInputWidth, $this->maxInputWidth);
        $itemValue = FormatService::formatTime($itemValue) . ' ' . LocalizationUtility::translate('tx_additionaltca_label.config_duration_hours');
        $id = StringUtility::getUniqueId('formengine-input-');
        $attributes = [
            'value' => $itemValue,
            'id' => $id,
            'class' => implode(' ', [
                'form-control',
                'hasDefaultValue',
                't3js-clearable',
                't3js-duration',
                'formengine-durationelement',
            ]),
            'data-formengine-validation-rules' => $this->getValidationDataAsJsonString([]),
            'data-formengine-input-params' => (string)json_encode([
                'field' => '',
                'evalList' => '',
                'is_in' => '',
            ]),
            'data-formengine-input-name' => $parameterArray['itemFormElName'],
        ];
        if ($this->isTypo3Version13OrNewer()) {
            unset($attributes['data-formengine-input-params']);
        }
        //
        // Is read only?!
        if ($config['readOnly'] ?? false) {
            $attributes['readonly'] = 'readonly';
            $attributes['disabled'] = 'disabled';
            $attributes['class'] = str_replace('t3js-clearable', '', $attributes['class']);
        }
        //
        // Load needed js library
        $resultArray['javaScriptModules'][] = JavaScriptModuleInstruction::create(
            '@codingms/additional-tca/Backend/FormEngine/Element/DurationElement.js'
        )->invoke('initialize', $id, $this->isToolbar(), $this->getStep());
        //
        // HTML
        $html = [];
        if ($this->isTypo3Version13OrNewer()) {
            $html[] = $this->generateInputLabelHtml($parameterArray, $fieldName);
        }
        $html[] = '<div class="formengine-field-item t3js-formengine-field-item">';
        if (isset($parameterArray['fieldConf']['description'])) {
            $description = $parameterArray['fieldConf']['description'];
            $html[] =   '<span class="formengine-field-item-description text-muted">' . $description . '</span>';
        }
        $html[] =   '<div class="form-wizards-wrap">';
        $html[] =       '<div class="form-wizards-element">';
        $html[] =           '<div class="input-group">';
        $html[] =               '<div class="form-control-wrap" style="max-width: ' . $this->formMaxWidth($size) . 'px;">';
        $html[] =                   '<input type="text"' . GeneralUtility::implodeAttributes($attributes, true) . ' />';
        $html[] =                   '<input type="hidden" readonly="readonly" name="' . $parameterArray['itemFormElName'] . '" value="' . htmlspecialchars($itemValue) . '" id="' . $id . '_hidden" />';
        $html[] =               '</div>';
        $html[] =           '</div>';
        $html[] =       '</div>';
        if (!empty($fieldWizardHtml)) {
            $html[] = '<div class="form-wizards-items-bottom">';
            $html[] = $fieldWizardHtml;
            $html[] = '</div>';
        }
        $html[] =   '</div>';
        $html[] = '</div>';
        $resultArray['html'] = implode(PHP_EOL, $html);
        return $resultArray;
    }

    /**
     * @return int
     */
    protected function getStep(): int
    {
        $step = intval($this->data['processedTca']['columns'][$this->data['fieldName']]['config']['fieldInformation']['AdditionalInfoPageTitle']['step'] ?? 15);
        if (!in_array($step, [5, 10, 15, 30])) {
            $step = 15;
        }
        return $step;
    }

    /**
     * @return bool
     */
    protected function isToolbar(): bool
    {
        return $this->data['processedTca']['columns'][$this->data['fieldName']]['config']['fieldInformation']['AdditionalInfoPageTitle']['toolbar'] ?? false;
    }
}
