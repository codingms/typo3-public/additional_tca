<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\Form\Element;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Form\Element\Traits\Typo3Version13Trait;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Result;
use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

class BadgeSuggested extends AbstractFormElement
{
    use Typo3Version13Trait;

    protected bool $renderBadgesOnly = false;

    /**
     * Handler for single nodes
     *
     * @return array<mixed> As defined in initializeResultArray() of AbstractNode
     */
    public function render(): array
    {
        $parameterArray = $this->data['parameterArray'];
        $renderData = $this->data['renderData'];
        $fieldName = $this->data['fieldName'];
        //
        // Fetch badges for suggestions
        $entries = $this->fetchAllBadges(
            (int)($this->data['effectivePid'] ?? $this->data['databaseRow']['pid']),
            $this->data['tableName'],
            $this->data['fieldName']
        );
        $marginFix = '';
        //
        $resultArray = $this->initializeResultArray();
        $config = $parameterArray['fieldConf']['config'];
        $size = MathUtility::forceIntegerInRange($config['size'] ?: $this->defaultInputWidth, $this->minimumInputWidth, $this->maxInputWidth);
        $id = StringUtility::getUniqueId('formengine-input-');
        $attributes = [
            'name' => $parameterArray['itemFormElName'],
            'value' => $parameterArray['itemFormElValue'],
            'id' => $id,
            'class' => implode(' ', [
                'form-control',
                'hasDefaultValue',
                't3js-clearable',
                't3js-duration',
                'formengine-badge-suggested',
            ]),
            'data-formengine-validation-rules' => $this->getValidationDataAsJsonString($config),
            'data-formengine-input-params' => (string)json_encode([
                'field' => '',
                'evalList' => '',
                'is_in' => '',
            ]),
            'data-formengine-input-name' => $parameterArray['itemFormElName'],
        ];
        if ($this->isTypo3Version13OrNewer()) {
            unset($attributes['data-formengine-input-params']);
        }
        //
        // Is read only?!
        if (isset($config['readOnly']) && $config['readOnly']) {
            $attributes['readonly'] = 'readonly';
            $attributes['class'] = str_replace('t3js-clearable', '', $attributes['class']);
        }
        //
        // Load needed js library
        $resultArray['javaScriptModules'][] = JavaScriptModuleInstruction::create(
            '@codingms/additional-tca/Backend/FormEngine/Element/BadgeSuggestedElement.js'
        )->invoke('initialize', $id);
        $badgeClass = 'badge badge-primary';
        //
        // HTML
        $html = [];
        if (!$this->renderBadgesOnly) {
            if ($this->isTypo3Version13OrNewer()) {
                $html[] = $this->generateInputLabelHtml($parameterArray, $fieldName);
            }
            $html[] = '<div class="formengine-field-item t3js-formengine-field-item">';
            $html[] = '<div class="form-wizards-wrap">';
            $html[] = '<div class="form-wizards-element">';
            $html[] = '<div class="form-control-wrap" style="' . $marginFix . '">';
            $html[] = '<input type="text"' . GeneralUtility::implodeAttributes($attributes, true) . ' />';
            if (count($entries) > 0) {
                $html[] = '<div class="form-wizards-element-scopes">';
                foreach ($entries as $entry) {
                    $html[] = '<a class="' . $badgeClass . '" href="#" style="border-radius: 2px">' . $entry . '</a>';
                }
                $html[] = '</div>';
            }
            $html[] = '</div>';
            $html[] = '</div>';
            $html[] = '</div>';
            $html[] = '</div>';
        } else {
            if (count($entries) > 0) {
                $name = $this->data['elementBaseName'];
                $onclick = 'document.querySelectorAll(\'[data-formengine-input-name=\\\'data' . $name . '\\\']\')[0].value = this.innerHTML;';
                $onclick .= 'document.querySelectorAll(\'[data-formengine-input-name=\\\'data' . $name . '\\\']\')[0].dispatchEvent(new Event(\'change\'));';
                $onclick .= 'return false;';
                $html[] = '<div class="form-wizards-element-scopes" style="' . $marginFix . '">';
                foreach ($entries as $entry) {
                    $html[] = '<a class="' . $badgeClass . '" onclick="' . $onclick . '" href="#" style="border-radius: 2px">' . $entry . '</a>';
                }
                $html[] = '</div>';
            }
        }
        $resultArray['html'] = implode(PHP_EOL, $html);
        return $resultArray;
    }

    /**
     * @param int $pageUid
     * @param string $table
     * @param string $field
     * @return array<mixed>
     * @throws Exception
     */
    public function fetchAllBadges($pageUid, $table, $field): array
    {
        if ((int)VersionNumberUtility::getCurrentTypo3Version() >= 13) {
            $pageUidParamType = \Doctrine\DBAL\ParameterType::INTEGER;
        } else {
            $pageUidParamType = \PDO::PARAM_INT;
        }
        $badges = [];
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable($table);
        $queryBuilder->select($field)
            ->from($table)
            ->where(
                $queryBuilder->expr()->eq(
                    'pid',
                    $queryBuilder->createNamedParameter($pageUid, $pageUidParamType)
                )
            );

        if (($result = $queryBuilder->executeQuery()) instanceof Result) {
            $records = [];
            if (method_exists($result, 'fetchAll')) {
                $records = $result->fetchAll();
            } elseif (method_exists($result, 'fetchAllAssociative')) {
                $records = $result->fetchAllAssociative();
            }
            foreach ($records as $record) {
                if (trim((string)$record[$field]) !== '') {
                    $badges[$record[$field]] = (string)$record[$field];
                }
            }
            sort($badges);
        }

        return $badges;
    }
}
