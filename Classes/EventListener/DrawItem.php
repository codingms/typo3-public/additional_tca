<?php

declare(strict_types=1);

namespace CodingMs\AdditionalTca\EventListener;

use CodingMs\AdditionalTca\Service\ContentTypeService;
use CodingMs\AdditionalTca\Service\FlexFormService;
use CodingMs\AdditionalTca\Service\InformationService;
use CodingMs\AdditionalTca\Service\ListTypeService;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

final class DrawItem
{
    /**
     * @param PageContentPreviewRenderingEvent $event
     */
    public function __invoke(PageContentPreviewRenderingEvent $event): void
    {
        if ($event->getTable() !== 'tt_content') {
            return;
        }
        $record = $event->getRecord();

        if ($record['CType'] === 'list' && (int)VersionNumberUtility::getCurrentTypo3Version() < 13) {
            // This case is only in version 12, when plugins are not already migrated to content-elements
            $extensionKey = ListTypeService::checkListType($record['list_type']);
            $flexFormSheets = FlexFormService::fetchSheetsByListType($record['list_type']);
        } else {
            $extensionKey = ContentTypeService::checkContentType($record['CType']);
            $flexFormSheets = FlexFormService::fetchSheetsByContentType($record['CType']);
        }

        if ($extensionKey !== '') {
            // Get general tab data
            $record['pi_flexform'] = (string)$record['pi_flexform'];
            //
            // Get the settings from flex form
            $flexformArray = GeneralUtility::xml2array($record['pi_flexform']);
            if (is_string($flexformArray)) {
                $flexformArray = [];
            }
            //
            // Get the flex form definition
            if (!empty($flexFormSheets)) {
                $event->setPreviewContent(self::buildPreviewTable($flexformArray, $flexFormSheets, $extensionKey));
            }
        }
    }

    public static function buildPreviewTable(array $flexformArray, array $flexFormSheets, string $extensionKey): string
    {
        $languageService = GeneralUtility::makeInstance(LanguageServiceFactory::class)
            ->createFromUserPreferences($GLOBALS['BE_USER']);
        //
        // Build the preview table
        $itemContent = '';
        $itemContent .= '<table class="table table-bordered table-striped">';
        foreach ($flexFormSheets as $sheetKey => $sheet) {
            //
            // Sheet/Tab headline
            $itemContent .= '<thead>';
            $itemContent .= '<tr>';
            if ((int)VersionNumberUtility::getCurrentTypo3Version() < 13) {
                // Ensure label is defined in sheetTitle node
                $sheet['ROOT']['sheetTitle'] = $sheet['ROOT']['TCEforms']['sheetTitle'] ?? $sheet['ROOT']['sheetTitle'];
            }
            $sheetTitle = $languageService->sL($sheet['ROOT']['sheetTitle'] ?? '');
            if ($sheetTitle) {
                $itemContent .= '<th colspan="2">' . $sheetTitle . InformationService::getDocumentationLink($extensionKey) . '</th>';
            } else {
                $itemContent .= '<th colspan="2"><i class="text-danger">sheetTitle missing</i>' . InformationService::getDocumentationLink($extensionKey) . '</th>';
            }
            $itemContent .= '</tr>';
            $itemContent .= '</thead>';
            //
            // Sheet/Tab settings
            foreach ($sheet['ROOT']['el'] as $key => $element) {
                if (isset($flexformArray['data'][$sheetKey]['lDEF'][$key])) {
                    $value = $flexformArray['data'][$sheetKey]['lDEF'][$key]['vDEF'];

                    if ((int)VersionNumberUtility::getCurrentTypo3Version() < 13) {
                        // Ensure label and config are defined in correct nodes
                        $element['label'] = $element['TCEforms']['label'] ?? $element['label'];
                        $element['config'] = $element['TCEforms']['config'] ?? $element['config'];
                    }
                    //
                    if ($element['config']['type'] === 'select' && $element['config']['renderType'] === 'selectSingle') {
                        //
                        // Single select
                        $valueLabel = '';
                        //
                        // Sometimes the values are not set!?
                        // Try to fetch automatically!
                        if (!isset($element['config']['items']) || empty($element['config']['items'])) {
                            if ($value > 0 && isset($element['config']['foreign_table'])) {
                                $record = BackendUtility::getRecord($element['config']['foreign_table'], $value);
                                $labelField = $GLOBALS['TCA'][$element['config']['foreign_table']]['ctrl']['label'];
                                if (is_array($record)) {
                                    $element['config']['items'][] = [
                                        $record[$labelField],
                                        $value
                                    ];
                                }
                            }
                        }
                        $element['config']['items'] = $element['config']['items'] ?? [];
                        foreach ($element['config']['items'] as $item) {
                            $itemLabel = $item['label'] ?? $item[0];
                            $itemValue = $item['value'] ?? $item[1];
                            if ($itemValue === $value) {
                                $valueLabel = '<span title="' . $itemValue . '">' . $languageService->sL($itemLabel) . '</span>';
                            }
                        }
                        $itemContent .= '<tr>';
                        $itemContent .= '<td style="width: 20%;">' . $languageService->sL($element['label']) . '</td>';
                        $itemContent .= '<td>' . $valueLabel . '</td>';
                        $itemContent .= '</tr>';
                        //
                    } elseif ($element['config']['type'] === 'select' && $element['config']['renderType'] === 'selectMultipleSideBySide') {
                        //
                        // Multiple select
                        $values = GeneralUtility::trimExplode(',', $value, true);
                        $valueLabels = [];
                        $implodeBy = ', ';
                        if (isset($element['config']['foreign_table'])) {
                            foreach ($values as $uid) {
                                $relationRow = BackendUtility::getRecord($element['config']['foreign_table'], $uid);
                                if (is_array($relationRow)) {
                                    $valueLabels[] = BackendUtility::getRecordTitle($element['config']['foreign_table'], $relationRow) . ' [' . $uid . ']';
                                }
                            }
                            $implodeBy = '<br />';
                        } elseif (isset($element['config']['items'])) {
                            foreach ($element['config']['items'] as $item) {
                                $itemLabel = $item['label'] ?? $item[0];
                                $itemValue = $item['value'] ?? $item[1];
                                if (in_array($itemValue, $values)) {
                                    $valueLabels[] = '<span title="' . $itemValue . '">' . $languageService->sL($itemLabel) . '</span>';
                                }
                            }
                        }
                        $itemContent .= '<tr>';
                        $itemContent .= '<td style="width: 20%;">' . $languageService->sL($element['label']) . '</td>';
                        $itemContent .= '<td>' . implode($implodeBy, $valueLabels) . '</td>';
                        $itemContent .= '</tr>';
                    } elseif ($element['config']['type'] === 'check') {
                        //
                        // Checkbox
                        $itemContent .= '<tr>';
                        $itemContent .= '<td style="width: 20%;">' . $languageService->sL($element['label']) . '</td>';
                        $itemContent .= '<td><span title="' . $value . '">' . ($value ? 'yes' : 'no') . ' </span></td>';
                        $itemContent .= '</tr>';
                    } else {
                        //
                        // Simple input
                        if (trim($value) !== '') {
                            $itemContent .= '<tr>';
                            $itemContent .= '<td style="width: 20%;">' . $languageService->sL($element['label']) . '</td>';
                            $itemContent .= '<td>' . $value . '</td>';
                            $itemContent .= '</tr>';
                        }
                    }
                }
            }
        }
        $itemContent .= '</table>';
        $itemContent .= InformationService::drawItemInformation($extensionKey);
        return $itemContent;
    }
}
