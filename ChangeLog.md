# Additional TCA Change-Log

## 2025-01-xx Release of version 1.16.x

*	[BUGFIX] Fix missing notice-text ind backend notice-field
*	[TASK] Add phone and hash trait for domain model



## 2025-01-24 Release of version 1.16.5

*	[BUGFIX] Fix content element preview in backend with inline xml



## 2025-01-19 Release of version 1.16.4

*	[TASK] Migrate plugin preview in backend
*	[TASK] Fix code style
*	[TASK] Update documentation pages for TYPO3 13



## 2025-01-14 Release of version 1.16.3

*	[TASK] Add helper for registering plugins as content elements
*	[TASK] Migrate icon registration and icon usage



## 2024-12-16 Release of version 1.16.2

*	[BUGFIX] Migrate TCA from t3editor to codeEditor



## 2024-11-27 Release of version 1.16.1

*	[BUGFIX] Fix flex-form labels in draw-item event



## 2024-11-15 Release of version 1.16.0

*	[TASK] Migrate to TYPO3 13, remove support for TYPO3 11



## 2024-11-15 Release of version 1.15.14

*	[BUGFIX] Fix adding and subctracting time/date in date-pickers



## 2024-11-15 Release of version 1.15.13

*	[BUGFIX] Fix parameter name in condition for l10n_parent



## 2024-10-21 Release of version 1.15.12

*	[BUGFIX] Fix hard-coded table name for l10n_parent



## 2024-10-17 Release of version 1.15.11

*	[BUGFIX] Add try blocks for dismiss js in order to avoid issues during frontend testing
*	[TASK] Add suggested extension information translations
*	[TASK] Add Attribute9Trait, Attribute10Trait, Attribute11Trait, Attribute12Trait



## 2024-08-22 Release of version 1.15.10

*	[TASK] Add possibility to turn off CURL verification, in order to avoid issues during codeception tests
*	[BUGFIX] Fix JavaScript initialization issue on DismissSuggestion script



## 2024-08-13 Release of version 1.15.9

*	[TASK] Add setUsername setting in Username trait
*	[TASK] Code clean up in JavaScript files



## 2024-05-23 Release of version 1.15.8

*	[TASK] Add images with alt and title only



## 2024-04-29 Release of version 1.15.7

*	[TASK] Add inline link for documentation and more informations
*	[TASK] Add phone field migration documentation



## 2024-03-12 Release of version 1.15.6

*	[TASK] Add nl2br in backend notive field



## 2024-02-25 Release of version 1.15.5

*	[BUGFIX] Restore TCA preset for frontend_user



## 2024-02-05 Release of version 1.15.4

*	[BUGFIX] Fix TCA datetime dbtype
*	[BUGFIX] Avoid accessing flex form array index on non-existing flex form



## 2024-01-15 Release of version 1.15.3

*	[BUGFIX] Fix version number



## 2024-01-13 Release of version 1.15.2

*	[TASK] Add TCA preset for a CSS field



## 2023-11-29 Release of version 1.15.1

*	[TASK] Add additional TCA config and Trait
*	[TASK] Optimize version conditions in PHP code



## 2023-11-22 Release of version 1.15.0

*	[FEATURE] Add a new custom field for phone



## 2023-11-01 Release of version 1.14.16

*	[TASK] Clean up documentation



## 2023-10-30 Release of version 1.14.15

*	[BUGFIX] Prevent warnings in drawitem content preview event



## 2023-10-18 Release of version 1.14.14

*	[TASK] Add more extensions to drawitem content preview event



## 2023-10-16 Release of version 1.14.13

*	[BUGFIX] Fix undefined eval array index in TCA
*	[TASK] Add more extensions to drawitem content preview event
*	[BUGFIX] Add default options for link preset



## 2023-10-15 Release of version 1.14.12

*	[TASK] Add more extensions to drawitem content preview event



## 2023-10-04 Release of version 1.14.11

*	[TASK] Migration stuff for link types
*	[TASK] Remove deprecated TCA for double field



## 2023-10-04 Release of version 1.14.10

*	[TASK] Remove deprecated TCA table_local field
*	[BUGFIX] Fix relation information in backend content element preview and add more compatible extensions



## 2023-10-03 Release of version 1.14.9

*	[TASK] Remove deprecated TCA in type group
*	[BUGFIX] Fix DrawItem listener
*	[TASK] Update T3editor/Modes.php
*	[TASK] Update JavaScriptModules.php
*	[TASK] Add HTML & Markdown example fields
*	[TASK] Add backend page module preview for portfolios extension



## 2023-09-04 Release of version 1.14.8

*	[TASK] Optimize documentation



## 2023-08-18 Release of version 1.14.7

*	[BUGFIX] Fix tt_content_drawItem event for TYPO3 12



## 2023-08-18 Release of version 1.14.5

*	[TASK] Migration of tt_content_drawItem hook for TYPO3 12



## 2023-08-16  Release of version 1.14.4

*	[BUGFIX] Fix PHP 7.4 issue in image trait



## 2023-08-15  Release of version 1.14.3

*	[BUGFIX] Fix allowed file types for images preset
*	[TASK] Optimize comma-separation in backend record preview



## 2023-08-08  Release of version 1.14.2

*	[TASK] Migration of checkbox TCA for TYPO3 12
*	[TASK] Migration of datetime field TCA for TYPO3 12
*	[TASK] Migration of required flag in TCA for TYPO3 12
*	[TASK] Migration of l10n_parent in TCA for TYPO3 12



## 2023-08-07  Release of version 1.14.1

*	[TASK] Extend documentation for new weight field



## 2023-07-24  Release of version 1.14.0

*	[FEATURE] Add a new custom field for weights



## 2023-07-19  Release of version 1.13.1

*	[TASK] Add strict types for import id trait
*	[BUGFIX] Fix markdown module for t3editor mode definitions to TYPO3 11
*	[TASK] Add getter for image path in image-trait
*	[TASK] Migrate markdown module for t3editor mode definitions to TYPO3 12
*	[BUGFIX] Remove unsupported markdown module for t3editor mode definitions
*	[BUGFIX] Add default for l10n_parent field in TCA
*	[BUGFIX] Fix badge suggested usage in TYPO3 11 and 12



## 2023-04-11  Release of version 1.13.0

*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10



## 2023-03-27  Release of version 1.12.4

*	[BUGFIX] Fix logical structure of information row



## 2023-03-21  Release of version 1.12.3

*	[BUGFIX] Fix BadgeSuggested element logic



## 2023-03-14  Release of version 1.12.2

*	[BUGFIX] Fix JavaScript logic in TYPO3 10/11
*	[BUGFIX] Add compatibility for TYPO3 10
*	[BUGFIX] Fix missing validation rules for BadgeSuggested



## 2023-03-09  Release of version 1.12.1

*	[BUGFIX] Fix application type condition in TYPO3 10



## 2023-03-08  Release of version 1.12.0

*	[FEATURE] Add control buttons for Duration field
*	[TASK] Execute PageRenderer-Hook only in BE-Context
*	[TASK] Add some hints in the documentation



## 2023-01-09  Release of version 1.11.1

*	[BUGFIX] Fix TCA title of the testing and example record
*	[TASK] Optimize code by using phpstan
*	[TASK] Add some traits for domain model property/getter/setter



## 2022-11-29  Release of version 1.11.0

*	[FEATURE] Add new Form element ExpandableTextarea
*	[TASK] Insert more example and testing fields in the example record



## 2022-10-22  Release of 1.10.2

*	[BUGFIX] Add DocumentService event in JavaScript initializations
*	[BUGFIX] Add missing description text in Currency and Percent field
*	[TASK] Reduce size of currency field



## 2022-10-11  Release of 1.10.1

*	[TASK] Insert button labels for date/time field widgets
*	[BUGFIX] Fix Flatpickr on change by using widget buttons
*	[BUGFIX] Fix JavaScript validation in email field for TYPO3 10
*	[TASK] Optimize error handling and information for update checks
*	[TASK] Add client side email validation
*	[BUGFIX] Extend JavaScript for Duration field, when a translated unit is in use



## 2022-08-21  Release of 1.10.0

*	[BUGFIX] Don't use a default value in Notice field
*	[BUGFIX] Fix additional date/time buttons, when no date/time is available
*	[FEATURE] Add option for divergent RTE configuration
*	[FEATURE] Extend Notice field with options and documentation



## 2022-08-13  Release of 1.9.0

*	[FEATURE] Add additional features for date and date/time Form-Engine field
*	[FEATURE] Add duration Form-Engine field
*	[TASK] Extend the documentation



## 2022-06-28  Release of 1.8.1

*	[BUGFIX] Fix accessing an undefined readonly array index



## 2022-06-26  Release of 1.8.0

*	[FEATURE] Add a new input field for colors with colorpicker



## 2022-05-27  Release of 1.7.4

*	[BUGFIX] Ensure that a default timestamp is an integer



## 2022-05-13  Release of 1.7.3

*	[BUGFIX] Fix not nullable price and percent render types



## 2022-05-05  Release of 1.7.2

*	[BUGFIX] Fix documentation for full presets



## 2022-05-03  Release of 1.7.1

*	[BUGFIX] Fix warnings of undefined dbType options



## 2022-04-19  Release of 1.7.0

*	[FEATURE] Add a new input field with suggested badges
*	[TASK] Add condition for language field in TYPO3 11



## 2022-04-09  Release of 1.6.4

*	[BUGFIX] Ensure translations are used from this extension



## 2022-04-09  Release of 1.6.3

*	[TASK] Insert option for default value
*	[TASK] Remove exclude definition from l10n_parent field in TCA
*	[TASK] Remove showRecordFieldList from TCA example
*	[BUGFIX] Add PHP dependency in ext_emconf.php



## 2022-04-02  Release of 1.6.2

*	[BUGFIX] Fix PHP 8 type issue with update message



## 2022-30-03  Release of 1.6.1

*	[BUGFIX] Fix issue with namespaces in traits



## 2022-03-03  Release of 1.6.0

*	[FEATURE] Add traits for Starttime and Endtime in models
*	[BUGFIX] Fix default configuration for starttime and endtime
*	[FEATURE] Add Notice TCA type, for displaying additional information and notices



## 2022-02-23  Release of 1.5.2

*	[BUGFIX] Fix TYPO3-TER md5 hash



## 2022-02-22  Release of 1.5.1

*	[FEATURE] Add domain model traits for properties
*	[FEATURE] Add generic draw item hook
*	[TASK] Remove configuration conditions for TYPO3 9 and older
*	[TASK] Replace LF by PHP_EOL
*	[TASK] Add documentations configuration and migrate into single documentation files
*	[TASK] Rise PHP version to 7.4
*	[TASK] Code clean up and refactoring



## 2022-01-03  Release of 1.5.0

*	[TASK] Add field configuration value for images and files
*	[TASK] Migration for TYPO3 11 and remove support for TYPO3 9
*	[TASK] Prepare some more tab labels



## 2021-06-05  Release of 1.4.2

*	[BUGFIX] Fix TCA eval of float values



## 2021-06-04  Release of 1.4.1

*	[TASK] Add an option file image file types in preset



## 2021-06-04  Release of 1.4.0

*	[FEATURE] Currency and Percent field supports now integer and float database fields



## 2021-05-30  Release of 1.3.6

*	[BUGFIX] Fix naming of frontend_user pre configuration to frontendUserSingle



## 2021-03-31  Release of 1.3.5

*	[BUGFIX] Fix configuration preset for links



## 2021-03-28  Release of 1.3.4

*	[BUGFIX] Fix render type for starttime and endtime
*	[TASK] Add configuration for HTML fields
*	[TASK] Add more default values for fields


## 2021-03-28  Release of 1.3.3

*	[BUGFIX] Migrate extension configuration utility
*	[BUGFIX] Remove clearable if field is readonly
*	[TASK] Add configuration examples in Readme



## 2020-09-22  Release of 1.3.2

*	[BUGFIX] Initialize form elements on document ready



## 2020-09-22  Release of 1.3.1

*	[BUGFIX] String to int conversion in form elements



## 2020-09-22  Release of 1.3.0

*	[FEATURE] Add field configuration preset for multiple images
*	[TASK] Add german translations
*	[BUGFIX] Correct lower and upper range for int field
*	[FEATURE] Lower and upper range for int field



## 2020-09-04  Release of 1.2.0

*	[FEATURE] Added Currency and Percent form elements
*	[BUGFIX] Changed fileSingle to inline type



## 2020-08-24  Release of 1.1.4

*	[TASK] Extend documentation
*	[TASK] Add labels for tabs
*	[TASK] Add frontend_user field
*	[TASK] Add default value for RTE field



## 2020-07-20  Release of 1.1.3

*	[BUGFIX] Add foreign_match_fields for image TCA presets



## 2020-07-17  Release of 1.1.2

*	[BUGFIX] Add default value for dateTime preset with timestamp



## 2020-07-02 Release of Version 1.1.1

*	[BUGFIX] Change information row priority in order to avoid conflicts



## 2020-07-02 Release of Version 1.1.0

*	[FEATURE] TCA preset for coordinate (latitude, longitude)
*	[TASK] Add more tab labels
*	[TASK] Extend documentation



## 2020-07-02 Release of Version 1.0.2

*	[TASK] Add tags to composer.json



## 2020-07-01 Release of Version 1.0.1

*	[TASK] Translate readme markdown see #1



## 2020-07-01 Release of Version 1.0.0

*	[TASK] Implement base features
