<?php

$extKey = 'additional_tca';
$table = 'tx_additionaltca_domain_model_example';
$lll = 'LLL:EXT:additional_tca/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'uid',
        'adminOnly' => true,
        'rootLevel' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'dividers2tabs' => true,
        'versioningWS' => false,
        'delete' => 'deleted',
        'enablecolumns' => [],
        'searchFields' => '',
        'typeicon_classes' => [
            'default' => 'mimetypes-x-content-additionaltca-example'
        ],
    ],
    'types' => [
        '1' => [
            'showitem' => '
            --div--;' . $lll . '.tab_defaults,
                information,
                string,
                int,
                email,
                color,
            --div--;' . $lll . '.tab_custom,
                --palette--;;currency,
                --palette--;;percent,
                --palette--;;weight,
                --palette--;;badge_suggested,
                phone,
            --div--;' . $lll . '.tab_date_times,
                --palette--;;date,
                --palette--;;datetime,
                --palette--;;duration,
            --div--;' . $lll . '.tab_expandable,
                --palette--;;expandable_textarea,
            --div--;' . $lll . '.tab_notices,
                notice,
                notice_lll,
                --palette--;;notice_bootstrap,
                notice_code,
            --div--;' . $lll . '.tab_html_markdown,
                html,
                markdown
            ',
        ],
    ],
    'palettes' => [
        'title_keywords' => ['showitem' => 'title, keywords'],
        'currency' => ['showitem' => 'currency_db_int, currency_db_float'],
        'percent' => ['showitem' => 'percent_db_int, percent_db_float'],
        'weight' => ['showitem' => 'weight_db_int, weight_db_float'],
        'badge_suggested' => ['showitem' => 'badge_suggested, --linebreak--, badge_suggested_notice'],
        'date' => ['showitem' => 'date_db_timestamp, date_db_date, --linebreak--, date_notice'],
        'datetime' => ['showitem' => 'datetime_db_timestamp, datetime_db_date, --linebreak--, datetime_notice'],
        'duration' => ['showitem' => 'duration, --linebreak--, duration_notice'],
        'expandable_textarea' => ['showitem' => 'expandable_textarea_open, expandable_textarea_closed, --linebreak--, expandable_textarea_small_open, expandable_textarea_small_closed'],
        'notice_bootstrap' => ['showitem' => 'notice_danger, notice_warning, notice_info, notice_success'],
    ],
    'columns' => [
        'information' => \CodingMs\AdditionalTca\Tca\Configuration::full('information', $table, $extKey),
        'string' => [
            'label' => $lll . '.string',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'int' => [
            'label' => $lll . '.int',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('int'),
        ],
        'email' => [
            'label' => $lll . '.email',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('email'),
        ],
        'color' => [
            'label' => $lll . '.color',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('color'),
        ],
        //
        // Custom ones
        'currency_db_int' => [
            'label' => $lll . '.currency_db_int',
            'description' => $lll . '.currency_db_int_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('currency'),
        ],
        'currency_db_float' => [
            'label' => $lll . '.currency_db_float',
            'description' => $lll . '.currency_db_float_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('currency', false, false, '', [
                'dbType' => 'float'
            ]),
        ],
        'percent_db_int' => [
            'label' => $lll . '.percent_db_int',
            'description' => $lll . '.percent_db_int_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('percent'),
        ],
        'percent_db_float' => [
            'label' => $lll . '.percent_db_float',
            'description' => $lll . '.percent_db_float_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('percent', false, false, '', [
                'dbType' => 'float'
            ]),
        ],
        'weight_db_int' => [
            'label' => $lll . '.weight_db_int',
            'description' => $lll . '.weight_db_int_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('weight'),
        ],
        'weight_db_float' => [
            'label' => $lll . '.weight_db_float',
            'description' => $lll . '.weight_db_float_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('weight', false, false, '', [
                'dbType' => 'float'
            ]),
        ],
        'badge_suggested' => [
            'label' => $lll . '.badge_suggested',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('badgeSuggested'),
        ],
        'badge_suggested_notice' => [
            'label' => $lll . '.notice',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => 'This field suggest you already available values in the same field in the database. You can simply assume existing ones only by clicking on the badge.',
            ]),
        ],
        'phone' => [
            'label' => $lll . '.phone',
            'description' => $lll . '.phone_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('phone'),
        ],
        //
        // Date times
        'date_db_timestamp' => [
            'label' => $lll . '.date_db_timestamp',
            'description' => $lll . '.date_db_timestamp_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('date', false, false, '', [
                'toolbar' => true,
                'dbType' => 'timestamp'
            ]),
        ],
        'date_db_date' => [
            'label' => $lll . '.date_db_date',
            'description' => $lll . '.date_db_date_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('date', false, false, '', [
                'toolbar' => true,
                'dbType' => 'date'
            ]),
        ],
        'date_notice' => [
            'label' => $lll . '.notice',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => 'The clock button inserts the current date. The +/- buttons selects the next and previous days. This additional buttons can be enabled with the parameter: toolbar.',
            ]),
        ],
        'datetime_db_timestamp' => [
            'label' => $lll . '.datetime_db_timestamp',
            'description' => $lll . '.datetime_db_timestamp_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('dateTime', false, false, '', [
                'toolbar' => true,
                'dbType' => 'timestamp'
            ]),
        ],
        'datetime_db_date' => [
            'label' => $lll . '.datetime_db_date',
            'description' => $lll . '.datetime_db_date_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('dateTime', false, false, '', [
                'toolbar' => true,
                'dbType' => 'datetime'
            ]),
        ],
        'datetime_notice' => [
            'label' => $lll . '.notice',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => 'The clock button inserts the current date/time. The +/- buttons increase or decrease the selected value by an own defined value (see parameter: step). This additional buttons can be enabled with the parameter: toolbar.',
            ]),
        ],
        'duration' => [
            'label' => $lll . '.duration',
            'description' => $lll . '.duration_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('duration', false, false, '', [
                'toolbar' => true,
                'step' => 15,
            ]),
        ],
        'duration_notice' => [
            'label' => $lll . '.notice',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => 'The +/- buttons adds/subtracts minutes by an own defined value (see parameter: step). This additional buttons can be enabled with the parameter: toolbar.',
            ]),
        ],
        //
        // Text area
        'expandable_textarea_open' => [
            'label' => $lll . '.expandable_textarea_open',
            'description' => $lll . '.expandable_textarea_open_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('expandableTextarea', false, false, '', [
                'expandedInitially' => true
            ]),
        ],
        'expandable_textarea_closed' => [
            'label' => $lll . '.expandable_textarea_closed',
            'description' => $lll . '.expandable_textarea_closed_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('expandableTextarea', false, false, '', [
                'expandedInitially' => false
            ]),
        ],
        'expandable_textarea_small_open' => [
            'label' => $lll . '.expandable_textarea_small_open',
            'description' => $lll . '.expandable_textarea_small_open_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('expandableTextarea', false, false, '', [
                'expandedInitially' => true,
                'size' => 'small'
            ]),
        ],
        'expandable_textarea_small_closed' => [
            'label' => $lll . '.expandable_textarea_small_closed',
            'description' => $lll . '.expandable_textarea_small_closed_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('expandableTextarea', false, false, '', [
                'expandedInitially' => false,
                'size' => 'small'
            ]),
        ],
        //
        // Notice variations
        'notice' => [
            'label' => $lll . '.notice',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => 'This is a simple text notice. This text is written within the TCA file itself. Such notice fields don\'t need a field in the database.',
            ]),
        ],
        'notice_lll' => [
            'label' => $lll . '.notice_lll',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'notice' => $lll . '.notice_lll_text',
            ]),
        ],
        'notice_danger' => [
            'label' => $lll . '.notice_danger',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'html' => '<i><b>Notice</b> danger</i>',
                'display' => 'danger',
            ]),
        ],
        'notice_warning' => [
            'label' => $lll . '.notice_warning',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'html' => '<i><b>Notice</b> warning</i>',
                'display' => 'warning',
            ]),
        ],
        'notice_info' => [
            'label' => $lll . '.notice_info',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'html' => '<i><b>Notice</b> info</i>',
                'display' => 'info',
            ]),
        ],
        'notice_success' => [
            'label' => $lll . '.notice_success',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'html' => '<i><b>Notice</b> success</i>',
                'display' => 'success',
            ]),
        ],
        'notice_code' => [
            'label' => $lll . '.notice_code',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('notice', false, false, '', [
                'html' => '<i><b>bold italic</b> only italic</i>',
                'display' => 'code',
            ]),
        ],
        //
        // HTML and Markdown
        'html' => [
            'label' => $lll . '.html',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('html'),
            'defaultExtras' => 'fixed-font:enable-tab',
        ],
        'markdown' => [
            'label' => $lll . '.markdown',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('markdown'),
            'defaultExtras' => 'fixed-font:enable-tab',
        ],
    ],
];

return $return;
