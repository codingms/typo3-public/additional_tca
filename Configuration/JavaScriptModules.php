<?php

return [
    'dependencies' => ['core', 'backend'],
    'tags' => [
        'backend.form',
    ],
    'imports' => [
        '@codingms/additional-tca/' => [
            'path' => 'EXT:additional_tca/Resources/Public/JavaScript/',
            'exclude' => [
                'EXT:additional_tca/Resources/Public/JavaScript/Contrib/',
            ],
        ],
        '@codingms/additional-tca/Backend/DismissSuggestions.js' => 'EXT:additional_tca/Resources/Public/JavaScript/Backend/DismissSuggestions.js',
        '@lezer/markdown' => 'EXT:additional_tca/Resources/Public/JavaScript/Contrib/@lezer/markdown.js',
        '@codemirror/lang-markdown' => 'EXT:additional_tca/Resources/Public/JavaScript/Contrib/@codemirror/lang-markdown.js'
    ],
];
