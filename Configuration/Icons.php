<?php

declare(strict_types=1);

use TYPO3\CMS\Core\Imaging\IconProvider\SvgSpriteIconProvider;

return [
    'mimetypes-x-content-additionaltca-example' => [
        'provider' => SvgSpriteIconProvider::class,
        'source' => 'EXT:additional_tca/Resources/Public/Icons/iconmonstr-tools-5.svg',
        'sprite' => 'EXT:additional_tca/Resources/Public/Icons/backend-sprites.svg#mimetypes-x-content-additionaltca-example',
    ],
];
