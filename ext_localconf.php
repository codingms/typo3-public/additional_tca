<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Register form engine fields
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1585222848] = [
    'nodeName' => 'Information',
    'priority' => '80',
    'class' => \CodingMs\AdditionalTca\Form\Element\Information::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1599211190] = [
    'nodeName' => 'Currency',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Currency::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1599211204] = [
    'nodeName' => 'Percent',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Percent::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1690214298] = [
    'nodeName' => 'Weight',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Weight::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1646140789] = [
    'nodeName' => 'Notice',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Notice::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1650378895] = [
    'nodeName' => 'BadgeSuggested',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\BadgeSuggested::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1646140790] = [
    'nodeName' => 'Duration',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Duration::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1599211191] = [
    'nodeName' => 'DateTime',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\DateTime::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1599211192] = [
    'nodeName' => 'Date',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Date::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1599211193] = [
    'nodeName' => 'Email',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Email::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1669029099] = [
    'nodeName' => 'ExpandableTextarea',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\ExpandableTextarea::class
];
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1699603268] = [
    'nodeName' => 'Phone',
    'priority' => '70',
    'class' => \CodingMs\AdditionalTca\Form\Element\Phone::class
];
