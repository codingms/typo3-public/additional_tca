import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";

class BadgeSuggestedElement {
    initialize(id) {
        DocumentService.ready().then(() => {
            let input = jQuery('#'+id);
            if (input) {
                let badges = jQuery(input.parents('.form-control-wrap')[0]).find('.form-wizards-element-scopes a.badge');
                if(badges.length > 0) {
                    jQuery.each(badges, function() {
                        let badge = jQuery(this);
                        badge.click(function(e) {
                            e.preventDefault();
                            input.val(badge.text()).change()
                        });
                    });
                }
            }
        });
    }
}

export default new BadgeSuggestedElement();
