import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";
import Icons from '@typo3/backend/icons.js';

class ExpandableTextareaElement {

    /**
     * Initializes the ExpandableTextareaElement.
     */
    initialize(id, expandedInitially) {
        DocumentService.ready().then(async () => {
            //
            // Add toggle visibility button under expandable-text field
            let $appendableTextarea = jQuery('#' + id);
            if ($appendableTextarea.length) {
                let $wrapper = $appendableTextarea.closest('.formengine-field-item');
                $wrapper.parent().append(
                    `
                    <div class="input-group-append">
                        <button class="btn btn-default" type="button" id="toggle-${id}" title="Hide" style="width: 100%">
                            ${await Icons.getIcon('actions-eye', Icons.sizes.small)}
                        </button>
                    </div>
                    `
                );
                let $collapse = jQuery('#toggle-' + id);
                $collapse.on('click', function () {
                    if ($wrapper.is(':visible')) {
                        $wrapper.slideUp();
                    } else {
                        $wrapper.slideDown();
                    }
                });
                if (!expandedInitially) {
                    $wrapper.slideUp();
                }
            }
        });
    };
}

export default new ExpandableTextareaElement();
