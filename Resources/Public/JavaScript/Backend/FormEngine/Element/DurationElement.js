import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";
import Icons from '@typo3/backend/icons.js';

class DurationElement {

    icons = {
        'actions-add': '',
        'actions-remove': ''
    };

    /**
     * Initializes the DurationElement.
     *
     * @param {String} selector
     * @param {Boolean} toolbar
     * @param {Number} step
     */
    async initialize(selector, toolbar, step) {
        for (const icon of Object.keys(this.icons)) {
            this.icons[icon] = await Icons.getIcon(icon, Icons.sizes.small);
        }
        //
        DocumentService.ready().then(() => {
            const field = jQuery('#' + selector);
            if (field.length > 0) {
                const fieldHidden = jQuery('#' + selector + '_hidden');
                const pattern = /(?<=\[).*?(?=\])/g;
                const fieldData = jQuery(fieldHidden).attr('name').match(pattern);
                const $wrapper = fieldHidden.closest('.input-group');
                const _this = this;

                //
                if (fieldHidden.length > 0) {
                    const value = field.val();
                    const readonly = field.attr('readonly');
                    //
                    if (typeof readonly === 'undefined' && toolbar) {
                        const idPlus = `duration-field-${fieldData[0]}-${fieldData[1]}-${fieldData[2]}-plus`;
                        const idMinus = `duration-field-${fieldData[0]}-${fieldData[1]}-${fieldData[2]}-minus`;
                        //
                        this.insertButton($wrapper, idPlus, 'actions-add', '+' + step + ' ' + TYPO3.lang.unit_minutes);
                        this.insertButton($wrapper, idMinus, 'actions-remove', '-' + step + ' ' + TYPO3.lang.unit_minutes);
                        //
                        jQuery(`#${idPlus}`).on('click', () => _this.modifyTime(field, fieldHidden, step, '+'));
                        jQuery(`#${idMinus}`).on('click', () => _this.modifyTime(field, fieldHidden, step, '-'));
                    }
                    //
                    const intValue = this.stringToInt(value, 'from initialize');
                    fieldHidden.val(intValue);
                    //
                    field.on('change', function() {
                        const intValue = _this.stringToInt(jQuery(this).val(), 'from onchange');
                        fieldHidden.val(intValue);
                    });
                    field.on('focus', function() {
                        const field = jQuery(this);
                        const value = field.val();
                        field.val(_this.removeUnitString(value));
                    });
                    field.on('blur', function() {
                        const field = jQuery(this);
                        const value = field.val();
                        field.val(_this.removeUnitString(value) + ' ' + _this.lastUsedUnit);
                    });
                } else {
                    top.TYPO3.Notification.error(
                        'Error',
                        'DurationElement.initialize: fieldHidden #' + selector + '_hidden not found!'
                    );
                }
            } else {
                top.TYPO3.Notification.error(
                    'Error',
                    'DurationElement.initialize: field #' + selector + ' not found!'
                );
            }
        });
    };

    /**
     * Duration to seconds.
     *
     * Example:
     * 1:30 -> 5400
     * 0:01 -> 60
     *
     * @param value
     */
    stringToInt(value) {
        if (typeof value === 'undefined') {
            top.TYPO3.Notification.error(
                'Error',
                'DurationElement.stringToInt: value is undefined!'
            );
        }
        value = this.removeUnitString(value);
        let seconds = 0;
        const parts = value.split(':');
        if (parts.length !== 2) {
            top.TYPO3.Notification.error('Error', 'DurationElement.stringToInt(' + value + ') failed!!');
        } else {
            const hours = parseInt(parts[0], 10);
            const minutes = parseInt(parts[1], 10);
            seconds = (hours * 60 * 60) + (minutes * 60);
        }
        return seconds;
    };

    lastUsedUnit = '';

    /**
     * Removes the unit string after time string
     *
     * Example:
     * 1:30 h -> 1:30
     * 0:01 Std. -> 0:01
     *
     * @param value
     */
    removeUnitString(value) {
        let valueParts = value.split(' ');
        if (valueParts.length > 1) {
            this.lastUsedUnit = valueParts[1];
        }
        return valueParts[0];
    };

    /**
     * Appends button to duration input element.
     *
     * @param {jQuery} $wrapper
     * @param {String} id
     * @param {String} icon
     * @param {String} title
     */
    insertButton($wrapper, id, icon, title) {
        $wrapper.append(
            `
            <div class="form-control-wrap">
                <div class="input-group-append">
                    <button style="padding: 7px" class="btn btn-default" type="button" id="${id}" title="${title}">
                        ${this.icons[icon]}
                    </button>
                </div>
            </div>
            `
        );
    };

    /**
     * Modifies value of the date time input.
     *
     * @param {jQuery} $visible
     * @param {jQuery} $hidden
     * @param {Number} step
     * @param {String} operator
     */
    modifyTime($visible, $hidden, step, operator) {
        const parts = new RegExp('(\\d+):(\\d+)').exec($visible.val());
        let hours = parts[1];
        let minutes = parts[2];
        let totalSeconds = hours * 3600 + minutes * 60
        let stepCalc = step * 60;

        if (operator === '+') {
            totalSeconds += stepCalc
        } else {
            totalSeconds -= stepCalc
        }

        $hidden.val(totalSeconds).change();
        $visible.val(this.secondsToHm(totalSeconds)).change();
    };

    /**
     * Converts seconds to h:m.
     *
     * @param {Number} seconds
     */
    secondsToHm(seconds) {
        if (seconds <= 0) {
            return `00:00 h`;
        }
        seconds = Number(seconds);
        let h = Math.floor(seconds / 3600).toString(),
            m = Math.floor(seconds % 3600 / 60).toString();

        return `${h.padStart(2, "0")}:${m.padStart(2, "0")} h`;
    }
}

export default new DurationElement();
