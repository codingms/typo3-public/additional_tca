import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";
import Icons from '@typo3/backend/icons.js';

class DateElement {

    icons = {
        'actions-clock': '',
        'actions-add': '',
        'actions-remove': ''
    };

    /**
     * Initializes the DateElement.
     *
     * @param {String} table
     * @param {String} uid
     * @param {String} field
     */
    async initialize(table, uid, field) {
        for (const icon of Object.keys(this.icons)) {
            this.icons[icon] = await Icons.getIcon(icon, Icons.sizes.small);
        }
        //
        DocumentService.ready().then(() => {

            const $hidden = jQuery(`input[name="data[${table}][${uid}][${field}]"`);
            const $wrapper = $hidden.closest('.input-group');
            const $visible = $wrapper.find('input').first();
            const _this = this;

            $wrapper.find('.icon-actions-edit-pick-date').closest('.input-group-btn').remove();

            const idNow = `date-field-${table}-${uid}-${field}-now`;
            const idPlus = `date-field-${table}-${uid}-${field}-plus`;
            const idMinus = `date-field-${table}-${uid}-${field}-minus`;

            this.insertButton($wrapper, idNow, 'actions-clock', TYPO3.lang.select_current_day);
            this.insertButton($wrapper, idPlus, 'actions-add', '+1 ' + TYPO3.lang.unit_day);
            this.insertButton($wrapper, idMinus, 'actions-remove', '-1 ' + TYPO3.lang.unit_day);
            jQuery('button', $wrapper).css('padding', '7px');

            jQuery(`#${idNow}`).on('click', () => {
                const values = _this.generateValues(new Date());
                $hidden.val(values.hidden);
                // Set visible date using flatpickr
                if ($visible[0]._flatpickr) {
                    $visible[0]._flatpickr.setDate(values.hidden, true);
                } else {
                    $visible.val(values.hidden);
                }
            });
            jQuery(`#${idPlus}`).on('click', () => _this.modifyTime($visible, $hidden, '+'));
            jQuery(`#${idMinus}`).on('click', () => _this.modifyTime($visible, $hidden, '-'));
        });
    };

    /**
     * Appends button to date time input element.
     *
     * @param {jQuery} $wrapper
     * @param {String} id
     * @param {String} icon
     * @param {String} title
     */
    insertButton($wrapper, id, icon, title) {
        $wrapper.append(
            `
            <div class="input-group-append">
                <button class="btn btn-default" type="button" id="${id}" title="${title}">
                    ${this.icons[icon]}
                </button>
            </div>
            `
        );
    };

    /**
     * Prepends 0 to the numbers that are less 10.
     *
     * @param {Number} value
     * @returns {String}
     */
    formatWithZeroes(value) {
        return value > 9 ? value : `0${value}`;
    }

    /**
     * Generates string values for inserting to visible and hidden TYPO3 inputs.
     *
     * @param {Date} date
     * @returns {{visible: string, hidden: string}}
     */
    generateValues(date) {
        const year = date.getFullYear();
        const month = this.formatWithZeroes(date.getMonth() + 1);
        const day = this.formatWithZeroes(date.getDate());
        return {
            hidden: `${year}-${month}-${day}`,
            visible: `${year}-${month}-${day}`
        };
    };

    /**
     * Modifies value of the date time input.
     *
     * @param {jQuery} $visible
     * @param {jQuery} $hidden
     * @param {String} operator
     */
    modifyTime ($visible, $hidden, operator) {
        let date = new Date();

        if ($visible.val()) {
            date = new Date($visible.val().replace('Z', ''));
        }

        if (operator === '+') {
            date.setDate(date.getDate() + 1);
        } else {
            date.setDate(date.getDate() - 1);
        }

        const values = this.generateValues(date);

        $hidden.val(values.hidden);
        // Set visible date using flatpickr
        if ($visible[0]._flatpickr) {
            $visible[0]._flatpickr.setDate(values.visible, true);
        } else {
            $visible.val(values.visible);
        }
    };

}

export default new DateElement();
