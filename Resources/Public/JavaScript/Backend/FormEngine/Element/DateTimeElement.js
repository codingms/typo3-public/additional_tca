import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";
import Icons from '@typo3/backend/icons.js';

class DateTimeElement {

    icons = {
        'actions-clock': '',
        'actions-add': '',
        'actions-remove': ''
    };

    /**
     * Initializes the DateTimeElement.
     *
     * @param {String} table
     * @param {String} uid
     * @param {String} field
     * @param {Number} step
     */
    async initialize(table, uid, field, step) {
        for (const icon of Object.keys(this.icons)) {
            this.icons[icon] = await Icons.getIcon(icon, Icons.sizes.small);
        }
        //
        DocumentService.ready().then(() => {
            if (![5, 10, 15, 30].includes(step)) {
                step = 15;
            }

            const $hidden = jQuery(`input[name="data[${table}][${uid}][${field}]"`);
            const $wrapper = $hidden.closest('.input-group');
            const $visible = $wrapper.find('input').first();
            const _this = this;

            $wrapper.find('.icon-actions-edit-pick-date').closest('.input-group-btn').remove();

            const idNow = `date-field-${table}-${uid}-${field}-now`;
            const idPlus = `date-field-${table}-${uid}-${field}-plus`;
            const idMinus = `date-field-${table}-${uid}-${field}-minus`;

            this.insertButton($wrapper, step, idNow, 'actions-clock', TYPO3.lang.select_current_day_time);
            this.insertButton($wrapper, step, idPlus, 'actions-add', '+' + step + ' ' + TYPO3.lang.unit_minutes);
            this.insertButton($wrapper, step, idMinus, 'actions-remove', '-' + step + ' ' + TYPO3.lang.unit_minutes);
            jQuery('button', $wrapper).css('padding', '7px');

            jQuery(`#${idNow}`).on('click', () => {
                const values = _this.generateValues(
                    _this.roundDate(step, new Date())
                );
                $hidden.val(values.hidden);
                // Set visible date using flatpickr
                if ($visible[0]._flatpickr) {
                    $visible[0]._flatpickr.setDate(values.hidden, true);
                } else {
                    $visible.val(values.hidden);
                }
            });
            jQuery(`#${idPlus}`).on('click', () => _this.modifyTime($visible, $hidden, step, '+'));
            jQuery(`#${idMinus}`).on('click', () => _this.modifyTime($visible, $hidden, step, '-'));
        });
    };

    /**
     * Appends button to date time input element.
     *
     * @param {jQuery} $wrapper
     * @param {Number} step
     * @param {String} id
     * @param {String} icon
     * @param {String} title
     */
    insertButton($wrapper, step, id, icon, title) {
        $wrapper.append(
            `
            <div class="input-group-append">
                <button class="btn btn-default" type="button" id="${id}" title="${title}">
                    ${this.icons[icon]}
                </button>
            </div>
            `
        );
    };

    /**
     * Prepends 0 to the numbers that are less 10.
     *
     * @param {Number} value
     * @returns {String}
     */
    formatWithZeroes(value) {
        return value > 9 ? value : `0${value}`;
    }

    /**
     * Generates string values for inserting to visible and hidden TYPO3 inputs.
     *
     * @param {Date} date
     * @returns {{visible: string, hidden: string}}
     */
    generateValues(date) {
        const year      = date.getFullYear();
        const month     = this.formatWithZeroes(date.getMonth() + 1);
        const day       = this.formatWithZeroes(date.getDate());
        const hours     = this.formatWithZeroes(date.getHours());
        const minutes   = this.formatWithZeroes(date.getMinutes());

        return {
            hidden: `${year}-${month}-${day}T${hours}:${minutes}:00Z`,
            visible: `${year}-${month}-${day}T${hours}:${minutes}:00`
        };
    };

    /**
     * Rounds given date to closest step.
     *
     * @param {Number} step
     * @param {Date} date
     * @returns {Date}
     */
    roundDate(step, date) {
        const coeff = 1000 * 60 * step;
        return new Date(Math.round(date.getTime() / coeff) * coeff);
    };

    /**
     * Modifies value of the date time input.
     *
     * @param {jQuery} $visible
     * @param {jQuery} $hidden
     * @param {Number} step
     * @param {String} operator
     */
    modifyTime($visible, $hidden, step, operator) {
        let date = new Date();

        if ($visible.val()) {
            date = new Date($visible.val().replace('Z', ''));
        }

        if (operator === '+' && $visible.val()) {
            date.setMinutes(date.getMinutes() + step);
        } else {
            date.setMinutes(date.getMinutes() - step);
        }

        const values = this.generateValues(
            this.roundDate(step, date)
        );

        $hidden.val(values.hidden).change();
        // Set visible date using flatpickr
        if ($visible[0]._flatpickr) {
            $visible[0]._flatpickr.setDate(values.visible, true);
        } else {
            $visible.val(values.visible);
        }
    };

}

export default new DateTimeElement();
