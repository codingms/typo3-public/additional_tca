import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";

class PhoneElement {
    initialize(selector, iso) {
        DocumentService.ready().then(() => {
            var field = jQuery('#' + selector);
            var fieldHidden = jQuery('#' + selector + '_hidden');
            var value = field.val();
            //
            const _this = this;
            if(value !== '') {
                field.val(_this.formatPhoneNumber(value, iso));
            } else {
                field.val('');
            }
            value = value.replace(/\s/g, '');
            fieldHidden.val(value);
            //
            field.on('change', function () {
                var value = field.val();
                value = _this.formatPhoneNumber(value, iso);
                value = value.replace(/\s/g, '');
                fieldHidden.val(value);
            });
            field.on('input', function () {
                this.value = this.value.replace(/[^0-9+]/g, '').replace(/(\+.*?)\+.*/g, '$1').replace(/^0[^.]/, '0');
            });
            field.on('focus', function () {
                var field = jQuery(this);
                var value = field.val();
                //value = value.replace('+', '');
                value = value.replace(/\s/g, '');
                field.val(value);
            });
            field.on('focusout', function () {
                var field = jQuery(this);
                var value = field.val();
                //value = value.replace('+', '');
                value = value.replace(/\s/g, '');
                if(value !== '') {
                    field.val(_this.formatPhoneNumber(value, iso));
                }
            });
        });
    };

    formatPhoneNumber(str, iso) {
        let phone = str.replace(/[^0-9]/g, '');
        phone = parseInt(phone).toString();
        if (phone === 'NaN') {
            return '';
        }
        let code = phone.slice(0,2);
        if (code !== '49') {
            code = this.getCountryCode(iso);
            phone = code + phone;
        }
        let part1 = phone.slice(2,5);
        let part2 = phone.slice(5,8);
        let part3 = phone.slice(8,11);
        let part4 = phone.slice(11);

        return '+' + code + ' ' + part1 + ' '+ part2 + ' '+ part3 + ' '+ part4;
    }

    getCountryCode(iso) {
        const codes = {
            "AF": {
                "country": "Afghanistan",
                "code": "93",
                "iso": "AF / AFG"
            },
            "AL": {
                "country": "Albania",
                "code": "355",
                "iso": "AL / ALB"
            },
            "DZ": {
                "country": "Algeria",
                "code": "213",
                "iso": "DZ / DZA"
            },
            "AS": {
                "country": "American Samoa",
                "code": "1-684",
                "iso": "AS / ASM"
            },
            "AD": {
                "country": "Andorra",
                "code": "376",
                "iso": "AD / AND"
            },
            "AO": {
                "country": "Angola",
                "code": "244",
                "iso": "AO / AGO"
            },
            "AI": {
                "country": "Anguilla",
                "code": "1-264",
                "iso": "AI / AIA"
            },
            "AQ": {
                "country": "Antarctica",
                "code": "672",
                "iso": "AQ / ATA"
            },
            "AG": {
                "country": "Antigua and Barbuda",
                "code": "1-268",
                "iso": "AG / ATG"
            },
            "AR": {
                "country": "Argentina",
                "code": "54",
                "iso": "AR / ARG"
            },
            "AM": {
                "country": "Armenia",
                "code": "374",
                "iso": "AM / ARM"
            },
            "AW": {
                "country": "Aruba",
                "code": "297",
                "iso": "AW / ABW"
            },
            "AU": {
                "country": "Australia",
                "code": "61",
                "iso": "AU / AUS"
            },
            "AT": {
                "country": "Austria",
                "code": "43",
                "iso": "AT / AUT"
            },
            "AZ": {
                "country": "Azerbaijan",
                "code": "994",
                "iso": "AZ / AZE"
            },
            "BS": {
                "country": "Bahamas",
                "code": "1-242",
                "iso": "BS / BHS"
            },
            "BH": {
                "country": "Bahrain",
                "code": "973",
                "iso": "BH / BHR"
            },
            "BD": {
                "country": "Bangladesh",
                "code": "880",
                "iso": "BD / BGD"
            },
            "BB": {
                "country": "Barbados",
                "code": "1-246",
                "iso": "BB / BRB"
            },
            "BY": {
                "country": "Belarus",
                "code": "375",
                "iso": "BY / BLR"
            },
            "BE": {
                "country": "Belgium",
                "code": "32",
                "iso": "BE / BEL"
            },
            "BZ": {
                "country": "Belize",
                "code": "501",
                "iso": "BZ / BLZ"
            },
            "BJ": {
                "country": "Benin",
                "code": "229",
                "iso": "BJ / BEN"
            },
            "BM": {
                "country": "Bermuda",
                "code": "1-441",
                "iso": "BM / BMU"
            },
            "BT": {
                "country": "Bhutan",
                "code": "975",
                "iso": "BT / BTN"
            },
            "BO": {
                "country": "Bolivia",
                "code": "591",
                "iso": "BO / BOL"
            },
            "BA": {
                "country": "Bosnia and Herzegovina",
                "code": "387",
                "iso": "BA / BIH"
            },
            "BW": {
                "country": "Botswana",
                "code": "267",
                "iso": "BW / BWA"
            },
            "BR": {
                "country": "Brazil",
                "code": "55",
                "iso": "BR / BRA"
            },
            "IO": {
                "country": "British Indian Ocean Territory",
                "code": "246",
                "iso": "IO / IOT"
            },
            "VG": {
                "country": "British Virgin Islands",
                "code": "1-284",
                "iso": "VG / VGB"
            },
            "BN": {
                "country": "Brunei",
                "code": "673",
                "iso": "BN / BRN"
            },
            "BG": {
                "country": "Bulgaria",
                "code": "359",
                "iso": "BG / BGR"
            },
            "BF": {
                "country": "Burkina Faso",
                "code": "226",
                "iso": "BF / BFA"
            },
            "BI": {
                "country": "Burundi",
                "code": "257",
                "iso": "BI / BDI"
            },
            "KH": {
                "country": "Cambodia",
                "code": "855",
                "iso": "KH / KHM"
            },
            "CM": {
                "country": "Cameroon",
                "code": "237",
                "iso": "CM / CMR"
            },
            "CA": {
                "country": "Canada",
                "code": "1",
                "iso": "CA / CAN"
            },
            "CV": {
                "country": "Cape Verde",
                "code": "238",
                "iso": "CV / CPV"
            },
            "KY": {
                "country": "Cayman Islands",
                "code": "1-345",
                "iso": "KY / CYM"
            },
            "CF": {
                "country": "Central African Republic",
                "code": "236",
                "iso": "CF / CAF"
            },
            "TD": {
                "country": "Chad",
                "code": "235",
                "iso": "TD / TCD"
            },
            "CL": {
                "country": "Chile",
                "code": "56",
                "iso": "CL / CHL"
            },
            "CN": {
                "country": "China",
                "code": "86",
                "iso": "CN / CHN"
            },
            "CX": {
                "country": "Christmas Island",
                "code": "61",
                "iso": "CX / CXR"
            },
            "CC": {
                "country": "Cocos Islands",
                "code": "61",
                "iso": "CC / CCK"
            },
            "CO": {
                "country": "Colombia",
                "code": "57",
                "iso": "CO / COL"
            },
            "KM": {
                "country": "Comoros",
                "code": "269",
                "iso": "KM / COM"
            },
            "CK": {
                "country": "Cook Islands",
                "code": "682",
                "iso": "CK / COK"
            },
            "CR": {
                "country": "Costa Rica",
                "code": "506",
                "iso": "CR / CRI"
            },
            "HR": {
                "country": "Croatia",
                "code": "385",
                "iso": "HR / HRV"
            },
            "CU": {
                "country": "Cuba",
                "code": "53",
                "iso": "CU / CUB"
            },
            "CW": {
                "country": "Curacao",
                "code": "599",
                "iso": "CW / CUW"
            },
            "CY": {
                "country": "Cyprus",
                "code": "357",
                "iso": "CY / CYP"
            },
            "CZ": {
                "country": "Czech Republic",
                "code": "420",
                "iso": "CZ / CZE"
            },
            "CD": {
                "country": "Democratic Republic of the Congo",
                "code": "243",
                "iso": "CD / COD"
            },
            "DK": {
                "country": "Denmark",
                "code": "45",
                "iso": "DK / DNK"
            },
            "DJ": {
                "country": "Djibouti",
                "code": "253",
                "iso": "DJ / DJI"
            },
            "DM": {
                "country": "Dominica",
                "code": "1-767",
                "iso": "DM / DMA"
            },
            "DO": {
                "country": "Dominican Republic",
                "code": "1-809, 1-829, 1-849",
                "iso": "DO / DOM"
            },
            "TL": {
                "country": "East Timor",
                "code": "670",
                "iso": "TL / TLS"
            },
            "EC": {
                "country": "Ecuador",
                "code": "593",
                "iso": "EC / ECU"
            },
            "EG": {
                "country": "Egypt",
                "code": "20",
                "iso": "EG / EGY"
            },
            "SV": {
                "country": "El Salvador",
                "code": "503",
                "iso": "SV / SLV"
            },
            "GQ": {
                "country": "Equatorial Guinea",
                "code": "240",
                "iso": "GQ / GNQ"
            },
            "ER": {
                "country": "Eritrea",
                "code": "291",
                "iso": "ER / ERI"
            },
            "EE": {
                "country": "Estonia",
                "code": "372",
                "iso": "EE / EST"
            },
            "ET": {
                "country": "Ethiopia",
                "code": "251",
                "iso": "ET / ETH"
            },
            "FK": {
                "country": "Falkland Islands",
                "code": "500",
                "iso": "FK / FLK"
            },
            "FO": {
                "country": "Faroe Islands",
                "code": "298",
                "iso": "FO / FRO"
            },
            "FJ": {
                "country": "Fiji",
                "code": "679",
                "iso": "FJ / FJI"
            },
            "FI": {
                "country": "Finland",
                "code": "358",
                "iso": "FI / FIN"
            },
            "FR": {
                "country": "France",
                "code": "33",
                "iso": "FR / FRA"
            },
            "PF": {
                "country": "French Polynesia",
                "code": "689",
                "iso": "PF / PYF"
            },
            "GA": {
                "country": "Gabon",
                "code": "241",
                "iso": "GA / GAB"
            },
            "GM": {
                "country": "Gambia",
                "code": "220",
                "iso": "GM / GMB"
            },
            "GE": {
                "country": "Georgia",
                "code": "995",
                "iso": "GE / GEO"
            },
            "DE": {
                "country": "Germany",
                "code": "49",
                "iso": "DE / DEU"
            },
            "GH": {
                "country": "Ghana",
                "code": "233",
                "iso": "GH / GHA"
            },
            "GI": {
                "country": "Gibraltar",
                "code": "350",
                "iso": "GI / GIB"
            },
            "GR": {
                "country": "Greece",
                "code": "30",
                "iso": "GR / GRC"
            },
            "GL": {
                "country": "Greenland",
                "code": "299",
                "iso": "GL / GRL"
            },
            "GD": {
                "country": "Grenada",
                "code": "1-473",
                "iso": "GD / GRD"
            },
            "GU": {
                "country": "Guam",
                "code": "1-671",
                "iso": "GU / GUM"
            },
            "GT": {
                "country": "Guatemala",
                "code": "502",
                "iso": "GT / GTM"
            },
            "GG": {
                "country": "Guernsey",
                "code": "44-1481",
                "iso": "GG / GGY"
            },
            "GN": {
                "country": "Guinea",
                "code": "224",
                "iso": "GN / GIN"
            },
            "GW": {
                "country": "Guinea-Bissau",
                "code": "245",
                "iso": "GW / GNB"
            },
            "GY": {
                "country": "Guyana",
                "code": "592",
                "iso": "GY / GUY"
            },
            "HT": {
                "country": "Haiti",
                "code": "509",
                "iso": "HT / HTI"
            },
            "HN": {
                "country": "Honduras",
                "code": "504",
                "iso": "HN / HND"
            },
            "HK": {
                "country": "Hong Kong",
                "code": "852",
                "iso": "HK / HKG"
            },
            "HU": {
                "country": "Hungary",
                "code": "36",
                "iso": "HU / HUN"
            },
            "IS": {
                "country": "Iceland",
                "code": "354",
                "iso": "IS / ISL"
            },
            "IN": {
                "country": "India",
                "code": "91",
                "iso": "IN / IND"
            },
            "ID": {
                "country": "Indonesia",
                "code": "62",
                "iso": "ID / IDN"
            },
            "IR": {
                "country": "Iran",
                "code": "98",
                "iso": "IR / IRN"
            },
            "IQ": {
                "country": "Iraq",
                "code": "964",
                "iso": "IQ / IRQ"
            },
            "IE": {
                "country": "Ireland",
                "code": "353",
                "iso": "IE / IRL"
            },
            "IM": {
                "country": "Isle of Man",
                "code": "44-1624",
                "iso": "IM / IMN"
            },
            "IL": {
                "country": "Israel",
                "code": "972",
                "iso": "IL / ISR"
            },
            "IT": {
                "country": "Italy",
                "code": "39",
                "iso": "IT / ITA"
            },
            "CI": {
                "country": "Ivory Coast",
                "code": "225",
                "iso": "CI / CIV"
            },
            "JM": {
                "country": "Jamaica",
                "code": "1-876",
                "iso": "JM / JAM"
            },
            "JP": {
                "country": "Japan",
                "code": "81",
                "iso": "JP / JPN"
            },
            "JE": {
                "country": "Jersey",
                "code": "44-1534",
                "iso": "JE / JEY"
            },
            "JO": {
                "country": "Jordan",
                "code": "962",
                "iso": "JO / JOR"
            },
            "KZ": {
                "country": "Kazakhstan",
                "code": "7",
                "iso": "KZ / KAZ"
            },
            "KE": {
                "country": "Kenya",
                "code": "254",
                "iso": "KE / KEN"
            },
            "KI": {
                "country": "Kiribati",
                "code": "686",
                "iso": "KI / KIR"
            },
            "XK": {
                "country": "Kosovo",
                "code": "383",
                "iso": "XK / XKX"
            },
            "KW": {
                "country": "Kuwait",
                "code": "965",
                "iso": "KW / KWT"
            },
            "KG": {
                "country": "Kyrgyzstan",
                "code": "996",
                "iso": "KG / KGZ"
            },
            "LA": {
                "country": "Laos",
                "code": "856",
                "iso": "LA / LAO"
            },
            "LV": {
                "country": "Latvia",
                "code": "371",
                "iso": "LV / LVA"
            },
            "LB": {
                "country": "Lebanon",
                "code": "961",
                "iso": "LB / LBN"
            },
            "LS": {
                "country": "Lesotho",
                "code": "266",
                "iso": "LS / LSO"
            },
            "LR": {
                "country": "Liberia",
                "code": "231",
                "iso": "LR / LBR"
            },
            "LY": {
                "country": "Libya",
                "code": "218",
                "iso": "LY / LBY"
            },
            "LI": {
                "country": "Liechtenstein",
                "code": "423",
                "iso": "LI / LIE"
            },
            "LT": {
                "country": "Lithuania",
                "code": "370",
                "iso": "LT / LTU"
            },
            "LU": {
                "country": "Luxembourg",
                "code": "352",
                "iso": "LU / LUX"
            },
            "MO": {
                "country": "Macau",
                "code": "853",
                "iso": "MO / MAC"
            },
            "MK": {
                "country": "Macedonia",
                "code": "389",
                "iso": "MK / MKD"
            },
            "MG": {
                "country": "Madagascar",
                "code": "261",
                "iso": "MG / MDG"
            },
            "MW": {
                "country": "Malawi",
                "code": "265",
                "iso": "MW / MWI"
            },
            "MY": {
                "country": "Malaysia",
                "code": "60",
                "iso": "MY / MYS"
            },
            "MV": {
                "country": "Maldives",
                "code": "960",
                "iso": "MV / MDV"
            },
            "ML": {
                "country": "Mali",
                "code": "223",
                "iso": "ML / MLI"
            },
            "MT": {
                "country": "Malta",
                "code": "356",
                "iso": "MT / MLT"
            },
            "MH": {
                "country": "Marshall Islands",
                "code": "692",
                "iso": "MH / MHL"
            },
            "MR": {
                "country": "Mauritania",
                "code": "222",
                "iso": "MR / MRT"
            },
            "MU": {
                "country": "Mauritius",
                "code": "230",
                "iso": "MU / MUS"
            },
            "YT": {
                "country": "Mayotte",
                "code": "262",
                "iso": "YT / MYT"
            },
            "MX": {
                "country": "Mexico",
                "code": "52",
                "iso": "MX / MEX"
            },
            "FM": {
                "country": "Micronesia",
                "code": "691",
                "iso": "FM / FSM"
            },
            "MD": {
                "country": "Moldova",
                "code": "373",
                "iso": "MD / MDA"
            },
            "MC": {
                "country": "Monaco",
                "code": "377",
                "iso": "MC / MCO"
            },
            "MN": {
                "country": "Mongolia",
                "code": "976",
                "iso": "MN / MNG"
            },
            "ME": {
                "country": "Montenegro",
                "code": "382",
                "iso": "ME / MNE"
            },
            "MS": {
                "country": "Montserrat",
                "code": "1-664",
                "iso": "MS / MSR"
            },
            "MA": {
                "country": "Morocco",
                "code": "212",
                "iso": "MA / MAR"
            },
            "MZ": {
                "country": "Mozambique",
                "code": "258",
                "iso": "MZ / MOZ"
            },
            "MM": {
                "country": "Myanmar",
                "code": "95",
                "iso": "MM / MMR"
            },
            "NA": {
                "country": "Namibia",
                "code": "264",
                "iso": "NA / NAM"
            },
            "NR": {
                "country": "Nauru",
                "code": "674",
                "iso": "NR / NRU"
            },
            "NP": {
                "country": "Nepal",
                "code": "977",
                "iso": "NP / NPL"
            },
            "NL": {
                "country": "Netherlands",
                "code": "31",
                "iso": "NL / NLD"
            },
            "AN": {
                "country": "Netherlands Antilles",
                "code": "599",
                "iso": "AN / ANT"
            },
            "NC": {
                "country": "New Caledonia",
                "code": "687",
                "iso": "NC / NCL"
            },
            "NZ": {
                "country": "New Zealand",
                "code": "64",
                "iso": "NZ / NZL"
            },
            "NI": {
                "country": "Nicaragua",
                "code": "505",
                "iso": "NI / NIC"
            },
            "NE": {
                "country": "Niger",
                "code": "227",
                "iso": "NE / NER"
            },
            "NG": {
                "country": "Nigeria",
                "code": "234",
                "iso": "NG / NGA"
            },
            "NU": {
                "country": "Niue",
                "code": "683",
                "iso": "NU / NIU"
            },
            "KP": {
                "country": "North Korea",
                "code": "850",
                "iso": "KP / PRK"
            },
            "MP": {
                "country": "Northern Mariana Islands",
                "code": "1-670",
                "iso": "MP / MNP"
            },
            "NO": {
                "country": "Norway",
                "code": "47",
                "iso": "NO / NOR"
            },
            "OM": {
                "country": "Oman",
                "code": "968",
                "iso": "OM / OMN"
            },
            "PK": {
                "country": "Pakistan",
                "code": "92",
                "iso": "PK / PAK"
            },
            "PW": {
                "country": "Palau",
                "code": "680",
                "iso": "PW / PLW"
            },
            "PS": {
                "country": "Palestine",
                "code": "970",
                "iso": "PS / PSE"
            },
            "PA": {
                "country": "Panama",
                "code": "507",
                "iso": "PA / PAN"
            },
            "PG": {
                "country": "Papua New Guinea",
                "code": "675",
                "iso": "PG / PNG"
            },
            "PY": {
                "country": "Paraguay",
                "code": "595",
                "iso": "PY / PRY"
            },
            "PE": {
                "country": "Peru",
                "code": "51",
                "iso": "PE / PER"
            },
            "PH": {
                "country": "Philippines",
                "code": "63",
                "iso": "PH / PHL"
            },
            "PN": {
                "country": "Pitcairn",
                "code": "64",
                "iso": "PN / PCN"
            },
            "PL": {
                "country": "Poland",
                "code": "48",
                "iso": "PL / POL"
            },
            "PT": {
                "country": "Portugal",
                "code": "351",
                "iso": "PT / PRT"
            },
            "PR": {
                "country": "Puerto Rico",
                "code": "1-787, 1-939",
                "iso": "PR / PRI"
            },
            "QA": {
                "country": "Qatar",
                "code": "974",
                "iso": "QA / QAT"
            },
            "CG": {
                "country": "Republic of the Congo",
                "code": "242",
                "iso": "CG / COG"
            },
            "RE": {
                "country": "Reunion",
                "code": "262",
                "iso": "RE / REU"
            },
            "RO": {
                "country": "Romania",
                "code": "40",
                "iso": "RO / ROU"
            },
            "RU": {
                "country": "Russia",
                "code": "7",
                "iso": "RU / RUS"
            },
            "RW": {
                "country": "Rwanda",
                "code": "250",
                "iso": "RW / RWA"
            },
            "BL": {
                "country": "Saint Barthelemy",
                "code": "590",
                "iso": "BL / BLM"
            },
            "SH": {
                "country": "Saint Helena",
                "code": "290",
                "iso": "SH / SHN"
            },
            "KN": {
                "country": "Saint Kitts and Nevis",
                "code": "1-869",
                "iso": "KN / KNA"
            },
            "LC": {
                "country": "Saint Lucia",
                "code": "1-758",
                "iso": "LC / LCA"
            },
            "MF": {
                "country": "Saint Martin",
                "code": "590",
                "iso": "MF / MAF"
            },
            "PM": {
                "country": "Saint Pierre and Miquelon",
                "code": "508",
                "iso": "PM / SPM"
            },
            "VC": {
                "country": "Saint Vincent and the Grenadines",
                "code": "1-784",
                "iso": "VC / VCT"
            },
            "WS": {
                "country": "Samoa",
                "code": "685",
                "iso": "WS / WSM"
            },
            "SM": {
                "country": "San Marino",
                "code": "378",
                "iso": "SM / SMR"
            },
            "ST": {
                "country": "Sao Tome and Principe",
                "code": "239",
                "iso": "ST / STP"
            },
            "SA": {
                "country": "Saudi Arabia",
                "code": "966",
                "iso": "SA / SAU"
            },
            "SN": {
                "country": "Senegal",
                "code": "221",
                "iso": "SN / SEN"
            },
            "RS": {
                "country": "Serbia",
                "code": "381",
                "iso": "RS / SRB"
            },
            "SC": {
                "country": "Seychelles",
                "code": "248",
                "iso": "SC / SYC"
            },
            "SL": {
                "country": "Sierra Leone",
                "code": "232",
                "iso": "SL / SLE"
            },
            "SG": {
                "country": "Singapore",
                "code": "65",
                "iso": "SG / SGP"
            },
            "SX": {
                "country": "Sint Maarten",
                "code": "1-721",
                "iso": "SX / SXM"
            },
            "SK": {
                "country": "Slovakia",
                "code": "421",
                "iso": "SK / SVK"
            },
            "SI": {
                "country": "Slovenia",
                "code": "386",
                "iso": "SI / SVN"
            },
            "SB": {
                "country": "Solomon Islands",
                "code": "677",
                "iso": "SB / SLB"
            },
            "SO": {
                "country": "Somalia",
                "code": "252",
                "iso": "SO / SOM"
            },
            "ZA": {
                "country": "South Africa",
                "code": "27",
                "iso": "ZA / ZAF"
            },
            "KR": {
                "country": "South Korea",
                "code": "82",
                "iso": "KR / KOR"
            },
            "SS": {
                "country": "South Sudan",
                "code": "211",
                "iso": "SS / SSD"
            },
            "ES": {
                "country": "Spain",
                "code": "34",
                "iso": "ES / ESP"
            },
            "LK": {
                "country": "Sri Lanka",
                "code": "94",
                "iso": "LK / LKA"
            },
            "SD": {
                "country": "Sudan",
                "code": "249",
                "iso": "SD / SDN"
            },
            "SR": {
                "country": "Suriname",
                "code": "597",
                "iso": "SR / SUR"
            },
            "SJ": {
                "country": "Svalbard and Jan Mayen",
                "code": "47",
                "iso": "SJ / SJM"
            },
            "SZ": {
                "country": "Swaziland",
                "code": "268",
                "iso": "SZ / SWZ"
            },
            "SE": {
                "country": "Sweden",
                "code": "46",
                "iso": "SE / SWE"
            },
            "CH": {
                "country": "Switzerland",
                "code": "41",
                "iso": "CH / CHE"
            },
            "SY": {
                "country": "Syria",
                "code": "963",
                "iso": "SY / SYR"
            },
            "TW": {
                "country": "Taiwan",
                "code": "886",
                "iso": "TW / TWN"
            },
            "TJ": {
                "country": "Tajikistan",
                "code": "992",
                "iso": "TJ / TJK"
            },
            "TZ": {
                "country": "Tanzania",
                "code": "255",
                "iso": "TZ / TZA"
            },
            "TH": {
                "country": "Thailand",
                "code": "66",
                "iso": "TH / THA"
            },
            "TG": {
                "country": "Togo",
                "code": "228",
                "iso": "TG / TGO"
            },
            "TK": {
                "country": "Tokelau",
                "code": "690",
                "iso": "TK / TKL"
            },
            "TO": {
                "country": "Tonga",
                "code": "676",
                "iso": "TO / TON"
            },
            "TT": {
                "country": "Trinidad and Tobago",
                "code": "1-868",
                "iso": "TT / TTO"
            },
            "TN": {
                "country": "Tunisia",
                "code": "216",
                "iso": "TN / TUN"
            },
            "TR": {
                "country": "Turkey",
                "code": "90",
                "iso": "TR / TUR"
            },
            "TM": {
                "country": "Turkmenistan",
                "code": "993",
                "iso": "TM / TKM"
            },
            "TC": {
                "country": "Turks and Caicos Islands",
                "code": "1-649",
                "iso": "TC / TCA"
            },
            "TV": {
                "country": "Tuvalu",
                "code": "688",
                "iso": "TV / TUV"
            },
            "VI": {
                "country": "U.S. Virgin Islands",
                "code": "1-340",
                "iso": "VI / VIR"
            },
            "UG": {
                "country": "Uganda",
                "code": "256",
                "iso": "UG / UGA"
            },
            "UA": {
                "country": "Ukraine",
                "code": "380",
                "iso": "UA / UKR"
            },
            "AE": {
                "country": "United Arab Emirates",
                "code": "971",
                "iso": "AE / ARE"
            },
            "GB": {
                "country": "United Kingdom",
                "code": "44",
                "iso": "GB / GBR"
            },
            "US": {
                "country": "United States",
                "code": "1",
                "iso": "US / USA"
            },
            "UY": {
                "country": "Uruguay",
                "code": "598",
                "iso": "UY / URY"
            },
            "UZ": {
                "country": "Uzbekistan",
                "code": "998",
                "iso": "UZ / UZB"
            },
            "VU": {
                "country": "Vanuatu",
                "code": "678",
                "iso": "VU / VUT"
            },
            "VA": {
                "country": "Vatican",
                "code": "379",
                "iso": "VA / VAT"
            },
            "VE": {
                "country": "Venezuela",
                "code": "58",
                "iso": "VE / VEN"
            },
            "VN": {
                "country": "Vietnam",
                "code": "84",
                "iso": "VN / VNM"
            },
            "WF": {
                "country": "Wallis and Futuna",
                "code": "681",
                "iso": "WF / WLF"
            },
            "EH": {
                "country": "Western Sahara",
                "code": "212",
                "iso": "EH / ESH"
            },
            "YE": {
                "country": "Yemen",
                "code": "967",
                "iso": "YE / YEM"
            },
            "ZM": {
                "country": "Zambia",
                "code": "260",
                "iso": "ZM / ZMB"
            },
            "ZW": {
                "country": "Zimbabwe",
                "code": "263",
                "iso": "ZW / ZWE"
            }
        }
        return codes[iso].code;
    }

}

export default new PhoneElement();
