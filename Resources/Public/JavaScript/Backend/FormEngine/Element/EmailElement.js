import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";

class EmailElement {
    registerCustomEvaluation(table, uid, field) {
        DocumentService.ready().then(() => {
            const hiddenField = jQuery('input[name="data[' + table + '][' + uid + '][' + field + ']"');
            const wrapper = hiddenField.closest('.form-group');
            const inputField = wrapper.find('input').first();
            inputField.on('keyup', function () {
                const value = jQuery(this).val();
                // Check whether a string is a valid email address
                // https://dev.to/dailydevtips1/vanilla-javascript-email-validation-21b3
                const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (regex.test(String(value).toLowerCase())) {
                    wrapper.removeClass('has-error');
                } else {
                    wrapper.addClass('has-error');
                }
            });
            inputField.on('blur', function () {
                const value = jQuery(this).val();
                // Check whether a string is a valid email address
                // https://dev.to/dailydevtips1/vanilla-javascript-email-validation-21b3
                const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (regex.test(String(value).toLowerCase())) {
                    wrapper.removeClass('has-error');
                } else {
                    wrapper.addClass('has-error');
                }
            });
        });
    }
}

export default new EmailElement();
