import DocumentService from '@typo3/core/document-service.js';
import Persistent from '@typo3/backend/storage/persistent.js';

class DismissSuggestions {
    initialize() {
        DocumentService.ready().then(() => {
            top.TYPO3.DismissSuggestions = this;
            var suggestions = Persistent.get('extension.suggestions');
            if (typeof suggestions === 'undefined') {
                suggestions = '1';
            }
            //
            // Display suggestions, if enabled
            if (suggestions === '1') {
                document.querySelectorAll('.alert-suggestion').forEach(function(element) {
                    element.classList.remove('d-none');
                });
            }
            //
            // Add dismiss button events
            document.querySelectorAll('button[data-dismiss-suggestion-button="1"]').forEach(button => {
                button.addEventListener('click', top.TYPO3.DismissSuggestions.deactivate);
            });
        });
    };

    deactivate() {
        Persistent.set('extension.suggestions', '0');
        document.querySelectorAll('.alert-suggestion').forEach(function(element) {
            element.style.display = 'none';
        });
    }

    activate() {
        Persistent.set('extension.suggestions', '1');
    }
}

export default new DismissSuggestions();
