# Additional-Tca Erweiterung für TYPO3

Diese Erweiterung hilft Dir dabei, das TCA übersichtlich und leicht wartbar zu halten.


**Features:**

*   Stellt Methoden für Tab-Beschriftungen bereit
*   Stellt Methoden für oft verwendete Felder wie sys_language_uid, hidden und mehr bereit (siehe Dokumentation)
*   Bietet Konfigurationsvoreinstellungen für den Markdown-t3editor
*   Form-Engine Feld für die Eingabe von Währungen
*   Form-Engine Feld für die Eingabe von Prozentwerten
*   Form-Engine Feld für die Eingabe von Uhrzeiten
*   Form-Engine Feld für die Eingabe von Gewichte
*   Form-Engine Feld für die Eingabe von Telefon
*   Form-Engine Feld für die Anzeige erweiterter Hinweise und Beschreibungen
*   Erweiterte Funktionen für das Datum Form-Engine Feld
*   Erweiterte Funktionen für das Datum/Uhrzeit Form-Engine Feld
*   Form-Engine Feld mit Bagdes mit dem man schnell wiederkehrende Begriffe auswählen kann
*   Ein- und ausklappbare Textarea, um Inhalte bei Bedarf zu verstecken

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Additional-Tca Dokumentation](https://www.coding.ms/documentation/typo3-additional-tca "Additional-Tca Dokumentation")
*   [Additional-Tca Bug-Tracker](https://gitlab.com/codingms/typo3-public/additional_tca/-/issues "Additional-Tca Bug-Tracker")
*   [Additional-Tca Repository](https://gitlab.com/codingms/typo3-public/additional_tca "Additional-Tca Repository")
*   [TYPO3 Additional-TCA](https://www.coding.ms/de/typo3-extensions/typo3-additional-tca/ "TYPO3 Additional-TCA")
*   [TYPO3 Additional-TCA Dokumentation](https://www.coding.ms/de/dokumentation/typo3-additional-tca "TYPO3 Additional-TCA Dokumentation")
*   [TYPO3 Additional TCA Extension Download](https://extensions.typo3.org/extension/additional_tca/ "TYPO3 Additional TCA Extension Download")
