# Phone field

This field can be used for phone values.

![Phone Form-Engine field](https://www.coding.ms/fileadmin/extensions/additional_tca/current/Documentation/Images/Phone.png)


## Definition (default with DE phone code)

```php
$table = 'tx_crm_domain_model_activityunit';
$lll = 'LLL:EXT:crm/Resources/Private/Language/locallang_db.xlf:' . $table;
return [
	'columns' => [
		'phone' => [
			'label' => $lll . '.phone',
			'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('phone'),
		],
	],
];
```

## Definition (with UK phone code)

```php
$table = 'tx_crm_domain_model_activityunit';
$lll = 'LLL:EXT:crm/Resources/Private/Language/locallang_db.xlf:' . $table;
return [
	'columns' => [
		'phone' => [
			'label' => $lll . '.phone',
			'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('phone', false, false, '', [
				'country' => 'UK'
			]),
		],
	],
];
```
## Migration tips

To update a phone number field (e.g. (0123) 456 789) to format '+49123456789':

```sql
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, '(', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, ')', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, ' ', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, '-', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, '/', '');
UPDATE tx_crm_domain_model_contact SET phone = CONCAT(REPLACE(LEFT(phone, 1), '0', '+49'),SUBSTRING(phone, 2)) WHERE phone LIKE '0%';
```
