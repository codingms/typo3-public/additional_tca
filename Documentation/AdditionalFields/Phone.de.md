# Telefon-Feld

Dieses Feld kann für Telefone verwendet werden.

![Weight Form-Engine field](https://www.coding.ms/fileadmin/extensions/additional_tca/current/Documentation/Images/Phone.png)


## Definition (Standardmäßig mit DE-Telefonvorwahl)

```php
$table = 'tx_crm_domain_model_activityunit';
$lll = 'LLL:EXT:crm/Resources/Private/Language/locallang_db.xlf:' . $table;
return [
	'columns' => [
		'phone' => [
			'label' => $lll . '.phone',
			'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('phone'),
		],
	],
];
```

## Definition (mit UK-Telefonvorwahl)

```php
$table = 'tx_crm_domain_model_activityunit';
$lll = 'LLL:EXT:crm/Resources/Private/Language/locallang_db.xlf:' . $table;
return [
	'columns' => [
		'phone' => [
			'label' => $lll . '.phone',
			'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('phone', false, false, '', [
				'country' => 'UK'
			]),
		],
	],
];
```

## Migrations-Tipps

So aktualisieren Sie ein Telefonnummernfeld (z. B. (0123) 456 789) im Format "+49123456789":

```sql
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, '(', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, ')', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, ' ', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, '-', '');
UPDATE tx_crm_domain_model_contact SET phone = REPLACE(phone, '/', '');
UPDATE tx_crm_domain_model_contact SET phone = CONCAT(REPLACE(LEFT(phone, 1), '0', '+49'),SUBSTRING(phone, 2)) WHERE phone LIKE '0%';
```
