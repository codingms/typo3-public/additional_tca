# Weight field

This field can be used for weight values.

![Weight Form-Engine field](https://www.coding.ms/fileadmin/extensions/additional_tca/current/Documentation/Images/Weight.png)


## Definition

```php
$table = 'tx_crm_domain_model_activityunit';
$lll = 'LLL:EXT:crm/Resources/Private/Language/locallang_db.xlf:' . $table;
return [
   'columns' => [
        'weight' => [
            'label' => $lll . '.weight',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('weight'),
        ],
    ],
];
```
