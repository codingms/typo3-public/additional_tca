<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function ($extKey) {
        //
        // Add some backend stylesheets and javascript
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess'][]
            = \CodingMs\AdditionalTca\Hooks\PageRendererHook::class . '->addJavaScriptAndStylesheets';
    },
    'additional_tca'
);
